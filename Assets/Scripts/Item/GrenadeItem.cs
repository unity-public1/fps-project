using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeItem : Test
{
    protected void Update()
    {
        Take(6, GrenadeItemSpawner.Instance);
    }

    protected override void Take(int weaponIndex, Spawner spawner)
    {
        Vector3 distanceToPlayer = player.position - transform.position;
        if (Input.GetKeyDown(KeyCode.F) && distanceToPlayer.magnitude <= pickupRange)
        {
            weaponHolder.weaponsObject[weaponIndex].GetComponent<GrenadeWeapon>().amount += ammoAmount;
            weaponHolder.weaponsObject[weaponIndex].GetComponent<GrenadeWeapon>().SetAmmoText();
            player.GetComponent<AudioSource>().PlayOneShot(pickupSound);
            PickUpUI.Instance.holder.SetActive(true);
            PickUpUI.Instance.pickUpText.text = "Picked up " + ammoAmount + " " + itemName;
            PickUpUI.Instance.ammoImage.sprite = image;
            PickUpUI.Instance.DisablePickupUI();
            spawner.Despawn(transform);
        }
    }
}
