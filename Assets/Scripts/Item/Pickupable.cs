using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.UI;

public class Test : MonoBehaviour
{   
    [SerializeField] public new Rigidbody rigidbody;
    [SerializeField] public AudioClip pickupSound;
    [SerializeField] public int ammoAmount = 30;
    [SerializeField] public float pickupRange = 4;
    [SerializeField] public string itemName;
    [SerializeField] public Sprite image;

    protected WeaponSelect weaponHolder => GameObject.Find("WeaponsHolder").GetComponent<WeaponSelect>();
    protected Transform player => GameObject.Find("Player").transform;

    protected void Start()
    {
        rigidbody.centerOfMass = Vector3.zero;
    }

    protected virtual void Take(int weaponIndex, Spawner spawner)
    {
        Vector3 distanceToPlayer = player.position - transform.position;
        if (Input.GetKeyDown(KeyCode.F) && distanceToPlayer.magnitude <= pickupRange)
        {
            int randomAmmoAmount = Random.Range(ammoAmount/2, ammoAmount);
            weaponHolder.weaponsObject[weaponIndex].GetComponent<Weapon>().weaponData.ammo += randomAmmoAmount;
            player.GetComponent<AudioSource>().PlayOneShot(pickupSound);
            PickUpUI.Instance.holder.SetActive(true);
            PickUpUI.Instance.pickUpText.text = "Picked up " + randomAmmoAmount + " " + itemName;
            PickUpUI.Instance.ammoImage.sprite = image;
            PickUpUI.Instance.DisablePickupUI();
            spawner.Despawn(transform);
        }
    }
}