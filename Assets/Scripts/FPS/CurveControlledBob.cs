using System;
using System.Collections;
using UnityEngine;

[Serializable]
public class CurveControlledBob
{
    [SerializeField] public float horizontalBobRange = 0.1f;
    [SerializeField] public float verticalBobRange = 0.1f;
    [SerializeField] public float verticalToHorizontalRatio = 2f;
    [SerializeField] public AnimationCurve bobCurve = new AnimationCurve(new Keyframe(0f, 0f), new Keyframe(0.5f, 1f),
                                                      new Keyframe(1f, 0f), new Keyframe(1.5f, -1f),
                                                      new Keyframe(2f, 0f)); // Sin curve for head bob

    protected float cyclePositionX;
    protected float cyclePositionY;
    protected float bobBaseInterval;
    protected float time;
    protected Vector3 originalCameraPosition;

    public void Setup(Camera camera, float bobBaseInterval)
    {
        this.bobBaseInterval = bobBaseInterval;
        originalCameraPosition = camera.transform.localPosition;

        // Get the length of the curve in time
        time = bobCurve[bobCurve.length - 1].time;
    }

    public Vector3 DoHeadBob(float speed)
    {
        float xPos = originalCameraPosition.x + (bobCurve.Evaluate(cyclePositionX) * horizontalBobRange);
        float yPos = originalCameraPosition.y + (bobCurve.Evaluate(cyclePositionY) * verticalBobRange);

        cyclePositionX += (speed * Time.deltaTime) / bobBaseInterval;
        cyclePositionY += ((speed * Time.deltaTime) / bobBaseInterval) * verticalToHorizontalRatio;

        if (cyclePositionX > time)
        {
            cyclePositionX = cyclePositionX - time;
        }
        if (cyclePositionY > time)
        {
            cyclePositionY = cyclePositionY - time;
        }

        return new Vector3(xPos, yPos, 0f);
    }
}