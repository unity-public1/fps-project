using System;
using System.Collections;
using UnityEngine;

[Serializable]
public class MouseLook
{
    [SerializeField] public float XSensitivity = 2f;
    [SerializeField] public float YSensitivity = 2f;
    [SerializeField] public bool clampVerticalRotation = true;
    [SerializeField] public float MinimumX = -90F;
    [SerializeField] public float MaximumX = 90F;
    [SerializeField] public bool smooth;
    [SerializeField] public float smoothTime = 5f;
    [SerializeField] public bool lockCursor = true;
    [SerializeField] public bool m_cursorIsLocked = true;

    protected Quaternion characterTargetRot;
    protected Quaternion cameraTargetRot;

    public void Init(Transform character, Transform camera)
    {
        characterTargetRot = character.localRotation;
        cameraTargetRot = camera.localRotation;
    }


    public void LookRotation(Transform character, Transform camera)
    {
        float yRot = Input.GetAxis("Mouse X") * XSensitivity;
        float xRot = Input.GetAxis("Mouse Y") * YSensitivity;

        characterTargetRot *= Quaternion.Euler (0f, yRot, 0f);
        cameraTargetRot *= Quaternion.Euler (-xRot, 0f, 0f);

        if(clampVerticalRotation)
            cameraTargetRot = ClampRotationAroundXAxis (cameraTargetRot);

        if(smooth)
        {
            character.localRotation = Quaternion.Slerp (character.localRotation, characterTargetRot,
                smoothTime * Time.deltaTime);
            camera.localRotation = Quaternion.Slerp (camera.localRotation, cameraTargetRot,
                smoothTime * Time.deltaTime);
        }
        else
        {
            character.localRotation = characterTargetRot;
            camera.localRotation = cameraTargetRot;
        }

        UpdateCursorLock();
    }

    public void SetCursorLock(bool value)
    {
        lockCursor = value;
        if(!lockCursor)

        {
            // Force unlock the cursor if the user disable the cursor locking helper
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    public void UpdateCursorLock()
    {
        // If the user set "lockCursor" we check & properly lock the cursos
        if (lockCursor)
            InternalLockUpdate();
    }

    protected void InternalLockUpdate()
    {
        if(Input.GetKeyUp(KeyCode.Escape))
        {
            m_cursorIsLocked = false;
        }
        else if(Input.GetMouseButtonUp(0))
        {
            m_cursorIsLocked = true;
        }

        if (m_cursorIsLocked)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else if (!m_cursorIsLocked)
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    protected Quaternion ClampRotationAroundXAxis(Quaternion q)
    {
        q.x /= q.w;
        q.y /= q.w;
        q.z /= q.w;
        q.w = 1.0f;

        float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan (q.x);

        angleX = Mathf.Clamp (angleX, MinimumX, MaximumX);

        q.x = Mathf.Tan (0.5f * Mathf.Deg2Rad * angleX);

        return q;
    }
}