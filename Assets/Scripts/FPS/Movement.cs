using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField] protected CharacterController characterController;
    [SerializeField] protected new Rigidbody rigidbody;
    [SerializeField] protected Transform _camera;

    protected Quaternion characterQuaternionRot;
    protected Quaternion cameraQuaternionRot;

    protected void Start()
    {
        cameraQuaternionRot = _camera.rotation;
        characterQuaternionRot = transform.rotation;
    }

    protected void Update()
    {
        float yRot = Input.GetAxis("Mouse X") * 2;
        float xRot = Input.GetAxis("Mouse Y") * 2;

        characterQuaternionRot *= Quaternion.Euler(0, yRot, 0);
        cameraQuaternionRot *= Quaternion.Euler(-xRot, 0, 0);

        //transform.localRotation = characterQuaternionRot;
        //_camera.localRotation = cameraQuaternionRot;

        transform.localRotation = Quaternion.Slerp(transform.localRotation, characterQuaternionRot, 5f * Time.deltaTime);
        _camera.localRotation = Quaternion.Slerp(_camera.localRotation, cameraQuaternionRot, 5f * Time.deltaTime);

    }

    protected void FixedUpdate()
    {
        Vector3 moveVector = Input.GetAxis("Vertical") * transform.forward + Input.GetAxis("Horizontal") * transform.right;
        characterController.Move(moveVector * Time.fixedDeltaTime * 10f);
    }
}
