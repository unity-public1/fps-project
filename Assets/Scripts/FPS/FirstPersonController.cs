using System;
using System.Collections;
using UnityEngine;

public class FirstPersonController : MonoBehaviour
{
    protected static FirstPersonController instance;
    public static FirstPersonController Instance => instance;

    [Header("Component")]
    [SerializeField] protected CharacterController characterController;
    [SerializeField] protected PlayerHealth playerHealth;
    [SerializeField] protected AudioSource FPSAudioSource;
    [SerializeField] protected AudioSource playerAudioSource;

    [Header("Atrribute")]
    [SerializeField] public bool isWalking;
    [SerializeField] protected float walkSpeed = 5f;
    [SerializeField] protected float runSpeed = 12.5f;
    [SerializeField] protected float stepInterval = 5f;
    [SerializeField] [Range(0f, 1f)] protected float runstepLenghten = 0.7f;
    [SerializeField] protected float jumpForce = 12.5f;
    [SerializeField] protected float stickToGroundForce = 10f;
    [SerializeField] protected float gravityMultiplier = 1.75f;

    [Header("Control")]
    [SerializeField] public MouseLook mouseLook;
    [SerializeField] protected FOVKick fovKick = new FOVKick();
    [SerializeField] protected CurveControlledBob headBob = new CurveControlledBob();
    [SerializeField] protected LerpControlledBob jumpBob = new LerpControlledBob();

    [Header("Sound")]
    [SerializeField] protected AudioClip[] footstepSounds;
    [SerializeField] protected AudioClip jumpSound;
    [SerializeField] protected AudioClip landSound;
    [SerializeField] protected AudioClip playerJumpSound;
    [SerializeField] protected AudioClip playerBreathSound;
    [SerializeField] protected AudioClip playerHeavyBreathSound;

    protected CollisionFlags collisionFlags;
    protected Vector2 input;
    protected Vector3 moveDir = Vector3.zero;
    protected Vector3 originalCameraPosition => camera.transform.localPosition;
    protected new Camera camera => Camera.main;
    protected bool jump;
    protected bool jumping;
    protected bool previouslyGrounded;
    protected float YRotation;
    protected float stepCycle = 0f;
    protected float nextStep;

    protected void Awake() => CreateSingleton();

    protected void Start()
    {
        Initialized();
    }

    protected void Initialized()
    {
        fovKick.Setup(camera);
        headBob.Setup(camera, stepInterval);
        mouseLook.Init(transform, camera.transform);
        nextStep = stepCycle/2f;
    }

    protected void Update()
    {
        RotateView();

        if (!jump)
        {
            jump = CrossPlatformInputManager.GetButtonDown("Jump");
        }

        if (!previouslyGrounded && characterController.isGrounded)
        {
            StartCoroutine(jumpBob.DoBobCycle());
            PlayLandingSound();
            moveDir.y = 0f;
            jumping = false;
        }

        if (!characterController.isGrounded && !jumping && previouslyGrounded)
        {
            moveDir.y = 0f;
        }

        previouslyGrounded = characterController.isGrounded;

        if (IsSprinting())
        {
            if (Input.GetKeyUp(KeyCode.LeftShift))
            {
                FPSAudioSource.PlayOneShot(playerBreathSound);
            }
        }
    }

    protected void FixedUpdate()
    {
        float speed;
        GetInput(out speed);
        // Always move along the camera forward as it is the direction that it being aimed at
        Vector3 desiredMove = transform.forward * input.y + transform.right * input.x;

        // Get a normal for the surface that is being touched to move along it
        RaycastHit hitInfo;
        Physics.SphereCast(transform.position, characterController.radius, Vector3.down, out hitInfo,
                            characterController.height/2f, Physics.AllLayers, QueryTriggerInteraction.Ignore);
        desiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfo.normal).normalized;

        moveDir.x = desiredMove.x * speed;
        moveDir.z = desiredMove.z * speed;
          
        if (characterController.isGrounded)
        {
            moveDir.y = -stickToGroundForce;

            if (jump)
            {
                FPSAudioSource.PlayOneShot(playerJumpSound);   
                moveDir.y = jumpForce;
                PlayJumpSound();
                jump = false;
                jumping = true;
            }
        }
        else
        {
            moveDir += Physics.gravity * gravityMultiplier * Time.fixedDeltaTime;
        }
        collisionFlags = characterController.Move(moveDir * Time.fixedDeltaTime);

        ProgressStepCycle(speed);
        UpdateCameraPosition(speed);

        mouseLook.UpdateCursorLock();
    }

    protected void PlayJumpSound()
    {
        playerAudioSource.clip = jumpSound;
        playerAudioSource.Play();
    }

    protected void PlayLandingSound()
    {
        playerAudioSource.clip = landSound;
        playerAudioSource.Play();
        nextStep = stepCycle + .5f;
    }

    protected void ProgressStepCycle(float speed)
    {
        if (characterController.velocity.sqrMagnitude > 0 && (input.x != 0 || input.y != 0))
        {
            stepCycle += (characterController.velocity.magnitude + (speed * (isWalking ? 1f : runstepLenghten))) * Time.fixedDeltaTime;
        }

        if (!(stepCycle > nextStep))
        {
            return;
        }

        nextStep = stepCycle + stepInterval;

        PlayFootStepAudio();
    }

    protected void PlayFootStepAudio()
    {
        if (!characterController.isGrounded)
        {
            return;
        }
        // Pick & play a random footstep sound from the array,
        // Excluding sound at index 0
        int n = UnityEngine.Random.Range(1, footstepSounds.Length);
        playerAudioSource.clip = footstepSounds[n];
        playerAudioSource.PlayOneShot(playerAudioSource.clip);
        // Move picked sound to index 0 so it's not picked next time
        footstepSounds[n] = footstepSounds[0];
        footstepSounds[0] = playerAudioSource.clip;
    }

    protected void UpdateCameraPosition(float speed)
    {
        Vector3 newCameraPosition;
        if (characterController.velocity.magnitude > 0 && characterController.isGrounded)
        {
            camera.transform.localPosition =
                headBob.DoHeadBob(characterController.velocity.magnitude +
                                    (speed*(isWalking ? 1f : runstepLenghten)));
            newCameraPosition = camera.transform.localPosition;
            newCameraPosition.y = camera.transform.localPosition.y - jumpBob.Offset();
        }
        else
        {
            newCameraPosition = camera.transform.localPosition;
            newCameraPosition.y = originalCameraPosition.y - jumpBob.Offset();
        }
        camera.transform.localPosition = newCameraPosition;
    }

    protected void GetInput(out float speed)
    {
        float horizontal = CrossPlatformInputManager.GetAxis("Horizontal");
        float vertical = CrossPlatformInputManager.GetAxis("Vertical");

        bool waswalking = isWalking;

        isWalking = !Input.GetKey(KeyCode.LeftShift);

        if (playerHealth.stamina > 0)
        {
            if (IsSprinting())
                playerHealth.StaminaConsume();
            else
                playerHealth.StaminaRecover();
        }
        else
        {
            if (!FPSAudioSource.isPlaying) FPSAudioSource.PlayOneShot(playerHeavyBreathSound);
            isWalking = true;
            StartCoroutine(SprintingCD(2f));
        }

        // Set the desired speed to be walking or running
        speed = isWalking && characterController.isGrounded || playerHealth.stamina <= 0 ? walkSpeed : runSpeed;
        input = new Vector2(horizontal, vertical);

        // Normalize input if it exceeds 1 in combined length:
        if (input.sqrMagnitude > 1)
            input.Normalize();

        // Handle speed change to give an fov kick
        // Only if the player is going to a run, is running and the fovkick is to be used
        if (isWalking != waswalking && characterController.velocity.sqrMagnitude > 0)
        {
            StopAllCoroutines();
            StartCoroutine(!isWalking ? fovKick.FOVKickUp() : fovKick.FOVKickDown());
        }
    }


    protected void RotateView()
    {
        mouseLook.LookRotation (transform, camera.transform);
    }


    protected void OnControllerColliderHit(ControllerColliderHit hit)
    {
        Rigidbody body = hit.collider.attachedRigidbody;
        // Dont move the rigidbody if the character is on top of it
        if (collisionFlags == CollisionFlags.Below)
        {
            return;
        }

        if (body == null || body.isKinematic)
        {
            return;
        }
        body.AddForceAtPosition(characterController.velocity*0.1f, hit.point, ForceMode.Impulse);
    }

    public bool IsSprinting()
    {
        return !isWalking && characterController.velocity.sqrMagnitude > 0;
    }

    protected IEnumerator SprintingCD(float cooldownTime)
    {
        yield return new WaitForSeconds(cooldownTime);
        playerHealth.StaminaRecover();
    }

    protected void CreateSingleton()
    {
        if (instance != null) return;
        instance = this;
    }
}