using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwayNBobScript : MonoBehaviour
{
    public CharacterController characterController;

    [Header("Sway")]
    [SerializeField] protected float step = 0.01f;
    [SerializeField] protected float maxStepDistance = 0.06f;
    protected Vector3 swayPos;

    [Header("Sway Rotation")]
    [SerializeField] protected float rotationStep = 4f;
    [SerializeField] protected float maxRotationStep = 5f;
    protected Vector3 swayEulerRot;

    [SerializeField] protected float smooth = 10f;
    [SerializeField] protected float smoothRot = 12f;

    [Header("Bobbing")]
    [SerializeField] protected float speedCurve;
    protected float curveSin { get => Mathf.Sin(speedCurve); }
    protected float curveCos { get => Mathf.Cos(speedCurve); }

    [SerializeField] protected Vector3 travelLimit = Vector3.one * 0.025f;
    [SerializeField] protected Vector3 bobLimit = Vector3.one * 0.01f;
    protected Vector3 bobPosition;

    [SerializeField] protected float bobExaggeration;

    [Header("Bob Rotation")]
    [SerializeField] protected Vector3 multiplier;
    [SerializeField] protected Vector3 runMultiplier;
    protected Vector3 bobEulerRotation;

    protected void Update()
    {
        GetInput();

        Sway();
        SwayRotation();
        BobOffset();
        BobRotation();

        CompositePositionRotation();
    }


    protected Vector2 walkInput;
    protected Vector2 lookInput;

    protected void GetInput()
    {
        walkInput.x = Input.GetAxis("Horizontal");
        walkInput.y = Input.GetAxis("Vertical");
        walkInput = walkInput.normalized;

        lookInput.x = Input.GetAxis("Mouse X");
        lookInput.y = Input.GetAxis("Mouse Y");
    }


    protected void Sway()
    {
        Vector3 invertLook = lookInput * -step;
        invertLook.x = Mathf.Clamp(invertLook.x, -maxStepDistance, maxStepDistance);
        invertLook.y = Mathf.Clamp(invertLook.y, -maxStepDistance, maxStepDistance);

        swayPos = invertLook;
    }

    protected void SwayRotation()
    {
        Vector2 invertLook = lookInput * -rotationStep;
        invertLook.x = Mathf.Clamp(invertLook.x, -maxRotationStep, maxRotationStep);
        invertLook.y = Mathf.Clamp(invertLook.y, -maxRotationStep, maxRotationStep);
        swayEulerRot = new Vector3(invertLook.y, invertLook.x, invertLook.x);
    }

    protected void CompositePositionRotation()
    {
        transform.localPosition = Vector3.Lerp(transform.localPosition, swayPos + bobPosition, Time.deltaTime * smooth);
        transform.localRotation = Quaternion.Slerp(transform.localRotation, Quaternion.Euler(swayEulerRot) * Quaternion.Euler(bobEulerRotation), Time.deltaTime * smoothRot);
    }

    protected void BobOffset()
    {
        speedCurve += Time.deltaTime * (characterController.isGrounded ? (Input.GetAxis("Horizontal") + Input.GetAxis("Vertical")) * bobExaggeration : 1f) + 0.01f;

        bobPosition.x = (curveCos * bobLimit.x * (characterController.isGrounded ? 1 : 0)) - (walkInput.x * travelLimit.x);
        bobPosition.y = (curveSin * bobLimit.y) - (Input.GetAxis("Vertical") * travelLimit.y);
        bobPosition.z = -(walkInput.y * travelLimit.z);
    }

    protected void BobRotation()
    {
        if (characterController.velocity != Vector3.zero && FirstPersonController.Instance.IsSprinting() && !Input.GetButton("Fire2"))
        {
            bobEulerRotation.x = (walkInput != Vector2.zero ? runMultiplier.x * (Mathf.Sin(2 * speedCurve)) : multiplier.x * (Mathf.Sin(2 * speedCurve) / 2));
            bobEulerRotation.y = (walkInput != Vector2.zero ? runMultiplier.y * curveCos : 0);
            bobEulerRotation.z = (walkInput != Vector2.zero ? runMultiplier.z * curveCos * walkInput.x : 0);
        }
        else
        {
            bobEulerRotation.x = (walkInput != Vector2.zero ? multiplier.x * (Mathf.Sin(2 * speedCurve)) : multiplier.x * (Mathf.Sin(2 * speedCurve) / 2));
            bobEulerRotation.y = (walkInput != Vector2.zero ? multiplier.y * curveCos : 0);
            bobEulerRotation.z = (walkInput != Vector2.zero ? multiplier.z * curveCos * walkInput.x : 0);
        }
    }
}