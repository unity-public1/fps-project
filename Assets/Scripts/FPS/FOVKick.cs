using System;
using System.Collections;
using UnityEngine;

[Serializable]
public class FOVKick
{
    [SerializeField] public Camera Camera;                           // Optional camera setup, if null the main camera will be used
    [SerializeField] public float FOVIncrease = 3f;                  // The amount the field of view increases when going into a run
    [SerializeField] public float timeToIncrease = 1f;               // The amount of time the field of view will increase over
    [SerializeField] public float timeToDecrease = 1f;               // The amount of time the field of view will take to return to its original size
    [SerializeField] public AnimationCurve increaseCurve;
    [HideInInspector] public float originalFov;                      // The original fov

    public void Setup(Camera camera)
    {
        CheckStatus(camera);

        Camera = camera;
        originalFov = camera.fieldOfView;
    }


    protected void CheckStatus(Camera camera)
    {
        if (camera == null)
        {
            throw new Exception("FOVKick camera is null, please supply the camera to the constructor");
        }

        if (increaseCurve == null)
        {
            throw new Exception(
                "FOVKick Increase curve is null, please define the curve for the field of view kicks");
        }
    }


    public void ChangeCamera(Camera camera)
    {
        Camera = camera;
    }


    public IEnumerator FOVKickUp()
    {
        float t = Mathf.Abs((Camera.fieldOfView - originalFov) / FOVIncrease);
        while (t < timeToIncrease)
        {
            Camera.fieldOfView = originalFov + (increaseCurve.Evaluate(t / timeToIncrease) * FOVIncrease);
            t += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }

    public IEnumerator FOVKickDown()
    {
        float t = Mathf.Abs((Camera.fieldOfView - originalFov) / FOVIncrease);
        while (t > 0)
        {
            Camera.fieldOfView = originalFov + (increaseCurve.Evaluate(t / timeToDecrease) * FOVIncrease);
            t -= Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        // Make sure that fov returns to the original size
        Camera.fieldOfView = originalFov;
    }
}