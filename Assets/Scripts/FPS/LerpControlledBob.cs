using System;
using System.Collections;
using UnityEngine;

[Serializable]
public class LerpControlledBob
{
    [SerializeField] public float bobDuration = 0.2f;
    [SerializeField] public float bobAmount = 0.1f;

    private float offset = 0f;

    // Provides the offset that can be used
    public float Offset()
    {
        return offset;
    }

    public IEnumerator DoBobCycle()
    {
        // Make the camera move down slightly
        float t = 0f;
        while (t < bobDuration)
        {
            offset = Mathf.Lerp(0f, bobAmount, t / bobDuration);
            t += Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }

        // Make it move back to neutral
        t = 0f;
        while (t < bobDuration)
        {
            offset = Mathf.Lerp(bobAmount, 0f, t / bobDuration);
            t += Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }
        offset = 0f;
    }
}
