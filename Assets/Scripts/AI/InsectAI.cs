using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InsectAI : EnemyAI
{
    protected override void Chase()
    {
        if (enemyHealth.isDead) return;
        if (playerHealth.isDead) return;

        if (DistanceToPlayer > 10)
        {
            transform.LookAt(player.position);
            agent.destination = player.position;
            animator.SetBool("Chase", true);
        }
        else if (DistanceToPlayer <= attackDistance)
        {
            Attack();
        }
    }

    protected override void Attack()
    {
        Vector3 lookAtPosition = player.position;
        lookAtPosition.y = transform.position.y;
        transform.LookAt(lookAtPosition);

        animator.SetBool("Chase", false);
        animator.SetTrigger("Attack");
        animator.SetInteger("AttackState", Random.Range(0, 3));
    }

    protected override void GetHit()
    {
        if (enemyHealth.isDead) return;

        if (enemyHealth.hP != enemyHealth.currentHP)
        {
            animator.SetTrigger("Hit");
            animator.SetInteger("HitState", 0);
            PlaySound(getHitSounds[Random.Range(0, getHitSounds.Length)]);
            enemyHealth.currentHP = enemyHealth.hP;
        }

        if (animator.GetCurrentAnimatorStateInfo(0).IsName("GetHit"))
            agent.velocity = Vector3.zero;
    }

    protected override void CheckDeath()
    {
        if (enemyHealth.isDead)
        {
            animator.SetTrigger("Death");
            animator.SetInteger("DeathState", 0);
            animator.SetBool("Chase", false);
            PlaySound(deathSounds[0]);
            this.enabled = false;
            agent.isStopped = true;          
        }

        if (playerHealth.isDead)
        {
            animator.SetBool("Chase", false);
            animator.SetBool("PlayerDeath", true);
        }
    }

    protected void DespawnAfterDeath()
    {
        InsectSpawner.Instance.DespawnAfterTime(transform.parent, 2f);
        EnemyWaveController.Instance.enemyRemain--;
        EnemyWaveController.Instance.killCount++;
        TimeAndScoreUI.Instance.scores += 100;
    }
}
