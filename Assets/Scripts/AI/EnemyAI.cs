using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAI : MonoBehaviour
{
    [SerializeField] protected NavMeshAgent agent;
    [SerializeField] protected Animator animator;
    [SerializeField] protected AudioSource audioSource;
    [SerializeField] protected float damage;
    [SerializeField] protected float attackDistance;
    [SerializeField] protected EnemyHealth enemyHealth;
    [SerializeField] protected PlayerHealth playerHealth => GameObject.Find("Player").GetComponent<PlayerHealth>();
    [SerializeField] protected Transform player => GameObject.Find("Player").transform;
    [SerializeField] protected AudioClip[] attackSounds;
    [SerializeField] protected AudioClip[] attackHitSounds;
    [SerializeField] protected AudioClip[] getHitSounds;
    [SerializeField] protected AudioClip[] deathSounds;

    protected float DistanceToPlayer => (transform.position - player.position).magnitude;

    protected void Update()
    {
        Chase();
        GetHit();
        CheckDeath();
    }

    protected virtual void Chase()
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Sit_Out_Of_ Ground")) return;
        if (enemyHealth.isDead) return;
        if (playerHealth.isDead) return;

        if (DistanceToPlayer > 10 && !animator.GetCurrentAnimatorStateInfo(0).IsName("Sit_Attack_2"))
        {
            agent.destination = player.position;
            animator.SetBool("Chase", true);
        }
        else if (DistanceToPlayer <= attackDistance)
        {
            Attack();
        }
    }

    protected virtual void Attack()
    {
        Vector3 lookAtPosition = player.position;
        lookAtPosition.y = transform.position.y;
        transform.LookAt(lookAtPosition);

        animator.SetBool("Chase", false);
        animator.SetTrigger("Attack");
        animator.SetInteger("AttackState", Random.Range(0, 6));
    }

    protected virtual void GetHit()
    {
        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Sit_Out_Of_ Ground")) return;
        if (enemyHealth.isDead) return;

        if (enemyHealth.hP != enemyHealth.currentHP)
        {
            animator.SetTrigger("Hit");
            animator.SetInteger("HitState", Random.Range(0, 4));
            PlaySound(getHitSounds[Random.Range(0, getHitSounds.Length)]);
            enemyHealth.currentHP = enemyHealth.hP;
        }

        if (animator.GetCurrentAnimatorStateInfo(0).IsName("Sit_Get_Hit_1") || animator.GetCurrentAnimatorStateInfo(0).IsName("Sit_Get_Hit_2"))
            agent.velocity = Vector3.zero;
    }

    protected virtual void CheckDeath()
    {
        if (enemyHealth.isDead)
        {
            animator.SetTrigger("Death");
            animator.SetInteger("DeathState", Random.Range(0, 3));
            animator.SetBool("Chase", false);
            PlaySound(deathSounds[0]);
            this.enabled = false;
            agent.isStopped = true;
        }

        if (playerHealth.isDead)
        {
            animator.SetBool("Chase", false);
            animator.SetBool("PlayerDeath", true);
        }
    }

    protected void SendDamage()
    {
        if (enemyHealth.isDead) return;
        if (DistanceToPlayer <= attackDistance && playerHealth.hP > 0)
        {
            if (playerHealth.armor > 0)
            {
                //CameraShake.Instance.SetShakeCamera(0.3f, 2);
                playerHealth.ArmorConsume(damage);
                PlaySound(attackHitSounds[Random.Range(0, attackHitSounds.Length)]);
                PlaySound(playerHealth.gruntSounds[Random.Range(0, playerHealth.gruntSounds.Length)]);
            }
            else
            {
                DamageIndicator.Instance.Show();
                CameraBlur.Instance.radius = 4.0f;
                //CameraShake.Instance.SetShakeCamera(0.3f, 2);
                playerHealth.TakeDamage(damage);
                PlaySound(attackHitSounds[Random.Range(0, attackHitSounds.Length)]);
                EnemyWaveController.Instance.damageTaken += damage;
            }
        }
    }

    protected void PlaySound(AudioClip clip)
    {
        audioSource.PlayOneShot(clip);
    }
}