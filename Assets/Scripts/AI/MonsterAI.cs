using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;

public class MonsterAI : EnemyAI
{
    protected void DespawnAfterDeath()
    {
        Monster_01Spawner.Instance.DespawnAfterTime(transform, 2f);
        EnemyWaveController.Instance.enemyRemain--;
        EnemyWaveController.Instance.killCount++;
        TimeAndScoreUI.Instance.scores += 300;
    }

    protected void ItemDrop()
    {
        if (!enemyHealth.isDead) return;
        int randomIndex = Random.Range(0, 5);
        GameObject newAmmo;
        Vector3 spawnPos = new Vector3(transform.position.x, transform.position.y + 2, transform.position.z);

        switch (randomIndex)
        {
            case 0:
                newAmmo = PistolAmmoSpawner.Instance.Spawn(PistolAmmoSpawner.Instance.holder);
                newAmmo.SetActive(true);
                newAmmo.transform.position = spawnPos;
                newAmmo.transform.rotation = Quaternion.identity;
                break;
            case 1:
                newAmmo = RifleAmmoSpawner.Instance.Spawn(RifleAmmoSpawner.Instance.holder);
                newAmmo.SetActive(true);
                newAmmo.transform.position = spawnPos;
                newAmmo.transform.rotation = Quaternion.identity;
                break;
            case 2:
                newAmmo = ShotgunAmmoSpawner.Instance.Spawn(ShotgunAmmoSpawner.Instance.holder);
                newAmmo.SetActive(true);
                newAmmo.transform.position = spawnPos;
                newAmmo.transform.rotation = Quaternion.identity;
                break;
            case 3:
                newAmmo = MGAmmoSpawner.Instance.Spawn(MGAmmoSpawner.Instance.holder);
                newAmmo.SetActive(true);
                newAmmo.transform.position = spawnPos;
                newAmmo.transform.rotation = Quaternion.identity;
                break;
            case 4:
                newAmmo = GrenadeItemSpawner.Instance.Spawn(GrenadeItemSpawner.Instance.holder);
                newAmmo.SetActive(true);
                newAmmo.transform.position = spawnPos;
                newAmmo.transform.rotation = Quaternion.identity;
                break;

            default: break;
        }
    }
}
