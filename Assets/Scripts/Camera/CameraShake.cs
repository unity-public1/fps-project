using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    protected static CameraShake instance;
    public static CameraShake Instance => instance;

    // How long the object should shake for.
    protected float SHAKE_MaxDuration = 0;
    protected float shakeDuration = 0f;

    // Amplitude of the shake. A larger value shakes the camera harder.
    protected float SHAKE_MaxAmount = 0.7f;
    protected float shakeAmount = 0.7f;
    protected float decreaseFactor = 1.0f;

    Vector3 originalPos;

    protected void Awake() => CreateSingleton();


    protected void Update()
    {
        if (shakeDuration > 0)
        {
            transform.localPosition = originalPos + Random.insideUnitSphere * shakeAmount;

            shakeDuration -= Time.deltaTime * decreaseFactor;
            shakeAmount = SHAKE_MaxAmount * shakeDuration / SHAKE_MaxDuration;
        }
        else
        {
            shakeDuration = 0f;
            transform.localPosition = originalPos;
        }
    }


    public void SetShakeCamera(float _shakeDuration, float _shakeAmount)
    {
        originalPos = transform.localPosition;
        SHAKE_MaxDuration = _shakeDuration;
        SHAKE_MaxAmount = _shakeAmount;

        shakeDuration = SHAKE_MaxDuration;
        shakeAmount = SHAKE_MaxAmount;
    }

    protected void CreateSingleton()
    {
        if (instance != null) return;
        instance = this;
    }
}
