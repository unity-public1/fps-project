using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWaveController : MonoBehaviour
{
    protected static EnemyWaveController instance;
    public static EnemyWaveController Instance => instance;

    [SerializeField] EnemyWave3 enemyWave3;
    [SerializeField] public int waves = 3;
    [SerializeField] public int enemyRemain = 0;
    [SerializeField] public int killCount = 0;
    [SerializeField] public float damageTaken = 0;
    protected bool levelClearPanelAfterTimeEnabled;

    protected void Awake() => CreateSingleton();

    protected void Update()
    {
        LevelClear();
    }

    protected void LevelClear()
    {
        if (enemyRemain == 0 && enemyWave3.endWave && !levelClearPanelAfterTimeEnabled)
        {
            levelClearPanelAfterTimeEnabled = true;
            StartCoroutine(EnableLevelClearPanelAfterTime());
        }
    }

    protected IEnumerator EnableLevelClearPanelAfterTime()
    {
        GameRoundUI.Instance.roundClear.SetActive(true);
        yield return new WaitForSeconds(3f);
        GameRoundUI.Instance.roundClear.SetActive(false);
        yield return new WaitForSeconds(3f);
        while (LevelClearPanel.Instance.backGround.alpha != 1f)
        {
            yield return new WaitForSeconds(Time.deltaTime * 0.5f);
            LevelClearPanel.Instance.backGround.alpha += Time.deltaTime * 0.5f;
        }
        LevelClearPanel.Instance.SetLevelClear();
        LevelClearPanel.Instance.resultHolder.SetActive(true);
        LevelClearPanel.Instance.resultShowed = true;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    protected void CreateSingleton()
    {
        if (instance != null) return;
        instance = this;
    }
}