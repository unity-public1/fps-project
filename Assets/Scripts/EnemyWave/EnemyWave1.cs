using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWave1 : MonoBehaviour
{
    [SerializeField] EnemyWave2 enemyWave2;
    [SerializeField] Transform[] spawnPositions;
    [HideInInspector] public bool endWave;

    protected void Start()
    {
        StartCoroutine(StartWaveAfterTime());
    }

    protected void Update()
    {
        if (EnemyWaveController.Instance.enemyRemain == 0 && endWave)
        {
            enemyWave2.gameObject.SetActive(true);
            gameObject.SetActive(false);
        }
    }

    protected IEnumerator StartWaveAfterTime()
    {
        while (GameRoundUI.Instance.timer > 10)
        {
            yield return new WaitForSeconds(1f);
            GameRoundUI.Instance.timer--;
        }
        GameRoundUI.Instance.roundStart.SetActive(true);

        while (GameRoundUI.Instance.timer > 0)
        {
            GameRoundUI.Instance.SetRoundStartText();
            yield return new WaitForSeconds(1f);
            GameRoundUI.Instance.timer--;
        }

        GameRoundUI.Instance.roundStart.SetActive(false);
        StartWave();
    }

    protected void StartWave()
    {
        GameRoundUI.Instance.SetRoundText("1");

        for (int i = 0; i < spawnPositions.Length; i++)
        {
            EnemyWaveController.Instance.enemyRemain++;
            GameObject enemy = RipperDogSpawner.Instance.Spawn(spawnPositions[i]);

            if (enemy == null) return;
            enemy.transform.position = spawnPositions[i].position;
            enemy.gameObject.SetActive(true);
            enemy.transform.SetParent(RipperDogSpawner.Instance.holder);
        }

        endWave = true;
    }
}
