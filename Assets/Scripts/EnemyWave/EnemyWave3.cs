using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWave3 : MonoBehaviour
{
    [SerializeField] Transform[] spawnPositions;
    [HideInInspector] public bool endWave;

    protected void OnEnable()
    {
        StartCoroutine(StartWaveAfterTime());
    }

    protected void Update()
    {
        if (EnemyWaveController.Instance.enemyRemain == 0 && endWave)
        {
            gameObject.SetActive(false);
        }
    }

    protected void StartWave()
    {
        GameRoundUI.Instance.SetRoundText("3");

        for (int i = 0; i < spawnPositions.Length; i++)
        {
            EnemyWaveController.Instance.enemyRemain++;
            GameObject enemy = Monster_01Spawner.Instance.Spawn(spawnPositions[i]);

            if (enemy == null) return;
            enemy.transform.position = spawnPositions[i].position;
            enemy.gameObject.SetActive(true);
            enemy.transform.SetParent(Monster_01Spawner.Instance.holder);
        }

        for (int i = 0; i < spawnPositions.Length; i++)
        {
            EnemyWaveController.Instance.enemyRemain++;
            GameObject enemy = RipperDogSpawner.Instance.Spawn(spawnPositions[i]);

            if (enemy == null) return;
            enemy.transform.position = spawnPositions[i].position;
            enemy.gameObject.SetActive(true);
            enemy.transform.SetParent(RipperDogSpawner.Instance.holder);
        }

        for (int i = 0; i < spawnPositions.Length; i++)
        {
            EnemyWaveController.Instance.enemyRemain++;
            GameObject enemy = InsectSpawner.Instance.Spawn(spawnPositions[i]);

            if (enemy == null) return;
            enemy.transform.position = spawnPositions[i].position;
            enemy.gameObject.SetActive(true);
            enemy.transform.SetParent(InsectSpawner.Instance.holder);
        }

        endWave = true;
    }

    protected IEnumerator StartWaveAfterTime()
    {
        GameRoundUI.Instance.roundClear.SetActive(true);
        yield return new WaitForSeconds(3f);
        GameRoundUI.Instance.roundClear.SetActive(false);
        GameRoundUI.Instance.timer = 13f;

        while (GameRoundUI.Instance.timer > 10)
        {
            yield return new WaitForSeconds(1f);
            GameRoundUI.Instance.timer--;
        }
        GameRoundUI.Instance.roundStart.SetActive(true);

        while (GameRoundUI.Instance.timer > 0)
        {
            GameRoundUI.Instance.SetRoundStartText();
            yield return new WaitForSeconds(1f);
            GameRoundUI.Instance.timer--;
        }

        GameRoundUI.Instance.roundStart.SetActive(false);
        StartWave();
    }
}
