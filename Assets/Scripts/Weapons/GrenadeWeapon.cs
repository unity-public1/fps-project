using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GrenadeWeapon : MonoBehaviour
{
    [Header("Component")]
    [SerializeField] protected Animator animator;
    [SerializeField] protected AudioSource audioSource;
    [SerializeField] protected GameObject grenadePrefab;
    [SerializeField] protected Transform throwPoint;
    [SerializeField] protected TMP_Text grenadeAmountText;

    [Header("Attribute")]
    [SerializeField] public int amount = 3;
    [SerializeField] protected float throwForce = 30f;

    [Header("Sounds")]
    [SerializeField] protected AudioClip[] audioClips;

    [Header("Animations")]
    [SerializeField] protected AnimationClip[] animationClips;

    protected void Update()
    {
        Throw();
        SetAmmoText();
    }

    protected void Throw()
    {
        if (amount <= 0) return;

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            animator.SetTrigger("Throw");
        }
    }

    protected void PerformThrow()
    {
        amount--;
        GameObject newGrenade = GrenadeSpawner.Instance.Spawn(grenadePrefab, throwPoint.position, throwPoint.rotation, GrenadeSpawner.Instance.holder);
        newGrenade.SetActive(true);
        newGrenade.transform.position = throwPoint.position;
        newGrenade.transform.rotation = throwPoint.rotation;
        newGrenade.GetComponent<Rigidbody>().velocity = Vector3.zero;
        newGrenade.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;

        newGrenade.GetComponent<Rigidbody>().AddForce((transform.forward * throwForce) + (transform.up * throwForce / 10), ForceMode.VelocityChange);
        newGrenade.GetComponent<Rigidbody>().AddTorque((transform.forward * throwForce) + (transform.up * throwForce / 10) * 10 * Random.Range(0.5f, 1), ForceMode.VelocityChange);
    }

    public void SetAmmoText()
    {
        grenadeAmountText.text = amount.ToString();
    }

    protected void PlayPullPinSound()
    {
        audioSource.PlayOneShot(audioClips[0]);
    }
}
