using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slugger : Weapon
{
    [SerializeField] protected new Animation animation;
    [SerializeField] protected AnimationClip[] animationClips;
    [SerializeField] protected AudioClip[] readySounds;
    [SerializeField] protected GameObject projectile;
    [SerializeField] protected Transform projectilePosition;

    protected void OnEnable()
    {
        animation.Stop();
        StartCoroutine(SetReady());
    }

    protected override void Fire()
    {
        if (weaponData.currentAmmo > 0)
        {
            if (weaponData.fireMode == FireMode.Automatic)
            {
                if (Input.GetButton("Fire1") && CanFire())
                {
                    HandleFire();
                }

                if (Input.GetButton("Fire1") && CanAutomaticReload())
                {
                    HandleReload();
                    PlayReloadSound();
                }

                if (Input.GetButtonUp("Fire1"))
                    PlayAfterFireEffect();
            }
            else if (weaponData.fireMode == FireMode.Single)
            {
                if (Input.GetButtonDown("Fire1") && CanFire())
                {
                    HandleFire();
                    PlayAfterFireEffect();
                }

                if (Input.GetButtonUp("Fire1"))
                    PlayAfterFireEffect();
            }
        }

        if (Input.GetButtonDown("Fire1") && CanAutomaticReload())
        {
            HandleReload();
            PlayReloadSound();
        }
        else if (Input.GetButtonDown("Fire1") && weaponData.currentAmmo <= 0)
        {
            PlayOutOfAmmoSound();
        }
    }

    protected override void HandleFire()
    {
        nextFire = Time.time + weaponData.fireRate;
        weaponData.currentAmmo--;

        animation[animationClips[1].name].speed = 2.5f;
        animation.Play(animationClips[1].name);

        PlayFireEffect();
        PlayFireSound();
        RecoilFire();
        CreateBulletTracer();
        UpdateCrosshair();
        SpawnProjectile();
    }

    protected void SpawnProjectile()
    {
        GameObject newProjectile = RocketSpawner.Instance.Spawn(RocketSpawner.Instance.holder);
        newProjectile.transform.position = projectilePosition.transform.position;
        newProjectile.transform.rotation = projectilePosition.transform.rotation;
        newProjectile.GetComponent<Rigidbody>().velocity = Vector3.zero;
        newProjectile.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        newProjectile.SetActive(true);
    }

    protected override bool CanFire()
    {
        return !animation.IsPlaying(animationClips[0].name) && Time.time > nextFire;
    }
    protected override void Reload()
    {
        if (Input.GetKeyDown(KeyCode.R) && CanReload())
        {
            PlayReloadSound();
            animation[animationClips[2].name].speed = 1.5f;
            animation.Play(animationClips[2].name);
        }
    }

    protected override void HandleReload()
    {
        animation.Play(animationClips[2].name);
    }

    protected override bool CanReload()
    {
        return !animation.IsPlaying(animationClips[0].name) && !animation.IsPlaying(animationClips[2].name) && weaponData.currentAmmo != weaponData.magazineSize && weaponData.ammo > 0;
    }

    protected override bool CanAutomaticReload()
    {
        return weaponData.currentAmmo <= 0 && weaponData.ammo > 0 && weaponData.reloadMode == ReloadMode.Automatic && !animation.IsPlaying(animationClips[0].name) && !animation.IsPlaying(animationClips[2].name);
    }


    protected override void EnableCrossHair()
    {
        if (isAiming || animation.IsPlaying(animationClips[2].name))
            crosshair.SetActive(false);
        else
            crosshair.SetActive(true);

        if (!FirstPersonController.Instance.isWalking && !animation.IsPlaying(animationClips[1].name))
            crosshair.GetComponent<Crosshair>().UpdateSize(2);
    }

    protected void SetAmmo()
    {
        int current;
        int result;

        current = weaponData.magazineSize - weaponData.currentAmmo;
        result = weaponData.ammo >= current ? current : weaponData.ammo;

        weaponData.currentAmmo += result;
        weaponData.ammo -= result;
    }

    protected virtual IEnumerator SetReady()
    {
        animation[animationClips[0].name].speed = 1.25f;
        animation.Play(animationClips[0].name);
        gunAudioSource.PlayOneShot(readySounds[Random.Range(0, readySounds.Length)]);
        yield return new WaitForSeconds(animationClips[0].length);
    }
}