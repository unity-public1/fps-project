using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shotgun : Weapon
{
    [SerializeField] protected AnimationClip[] animationClips;
    [SerializeField] protected AudioClip zatvorSounds;
    [SerializeField] protected ParticleGroupEmitter shellEmiter;
    [SerializeField] protected int bullets = 10;
    [SerializeField] protected float randomAngle = 5f;

    protected override void Fire()
    {
        if (weaponData.currentAmmo > 0)
        {
            if (weaponData.fireMode == FireMode.Automatic)
            {
                if (Input.GetButton("Fire1") && CanFire())
                {
                    HandleFire();
                }

                if (Input.GetButtonUp("Fire1"))
                    PlayAfterFireEffect();
            }
            else if (weaponData.fireMode == FireMode.Single)
            {
                if (Input.GetButtonDown("Fire1") && Time.time > nextFire)
                {
                    HandleFire();
                }

                if (Input.GetButtonUp("Fire1"))
                    PlayAfterFireEffect();
            }            
        }

        if (Input.GetButtonDown("Fire1") && CanAutomaticReload())
        {
            HandleReload();
        }
        else if (Input.GetButtonDown("Fire1") && weaponData.currentAmmo <= 0)
        {
            PlayOutOfAmmoSound();
        }
    }

    protected override bool CanFire()
    {
        return !animator.GetCurrentAnimatorStateInfo(0).IsName("Recharge_beginning") && Time.time > nextFire;
    }

    protected override void CreateImpactEffect()
    {   
        for (int i = 0; i < bullets; i++)
        {
            Vector3 direction = Camera.main.transform.forward;
            direction = Quaternion.AngleAxis(Random.Range(-randomAngle, randomAngle), Camera.main.transform.up) * direction;
            direction = Quaternion.AngleAxis(Random.Range(-randomAngle, randomAngle), Camera.main.transform.right) * direction;

            Ray r = new Ray(recoilTransform.position, direction);

            if (Physics.Raycast(r, out hit, weaponData.bulletDistance))
            {
                GameObject effect = GetImpactEffect(hit.transform.gameObject);
                if (effect == null) return;

                switch (effect.name)
                {
                    case "BrickImpact":
                        GameObject BrickIstance = GetSpawner("BrickImpact");
                        InitImpact(BrickIstance);
                        StartCoroutine(ImpactDespawn(5f, BrickImpactSpawner.Instance, BrickIstance.transform));
                        break;
                    case "ConcreteImpact":
                        GameObject ConcreteIstance = GetSpawner("ConcreteImpact");
                        InitImpact(ConcreteIstance);
                        StartCoroutine(ImpactDespawn(5f, ConcreteImpactSpawner.Instance, ConcreteIstance.transform));
                        break;
                    case "DirtImpact":
                        GameObject DirtIstance = GetSpawner("DirtImpact");
                        InitImpact(DirtIstance);
                        StartCoroutine(ImpactDespawn(5f, DirtImpactSpawner.Instance, DirtIstance.transform));
                        break;
                    case "FoliageImpact":
                        GameObject FoliageIstance = GetSpawner("FoliageImpact");
                        InitImpact(FoliageIstance);
                        StartCoroutine(ImpactDespawn(5f, FoliageImpactSpawner.Instance, FoliageIstance.transform));
                        break;
                    case "GlassImpact":
                        GameObject GlassIstance = GetSpawner("GlassImpact");
                        InitImpact(GlassIstance);
                        StartCoroutine(ImpactDespawn(5f, GlassImpactSpawner.Instance, GlassIstance.transform));
                        break;
                    case "MetalImpact":
                        GameObject MetalIstance = GetSpawner("MetalImpact");
                        InitImpact(MetalIstance);
                        StartCoroutine(ImpactDespawn(5f, MetalImpactSpawner.Instance, MetalIstance.transform));
                        break;
                    case "PlasterImpact":
                        GameObject PlasterIstance = GetSpawner("PlasterImpact");
                        InitImpact(PlasterIstance);
                        StartCoroutine(ImpactDespawn(5f, PlasterImpactSpawner.Instance, PlasterIstance.transform));
                        break;
                    case "RockImpact":
                        GameObject RockIstance = GetSpawner("RockImpact");
                        InitImpact(RockIstance);
                        StartCoroutine(ImpactDespawn(5f, RockImpactSpawner.Instance, RockIstance.transform));
                        break;
                    case "SkinImpact":
                        GameObject SkinIstance = GetSpawner("SkinImpact");
                        InitImpact(SkinIstance);
                        StartCoroutine(ImpactDespawn(5f, SkinImpactSpawner.Instance, SkinIstance.transform));
                        break;
                    case "WaterImpact":
                        GameObject WaterIstance = GetSpawner("WaterImpact");
                        InitImpact(WaterIstance);
                        StartCoroutine(ImpactDespawn(5f, WaterImpactSpawner.Instance, WaterIstance.transform));
                        break;
                    case "WoodImpact":
                        GameObject WoodIstance = GetSpawner("WoodImpact");
                        InitImpact(WoodIstance);
                        StartCoroutine(ImpactDespawn(5f, WoodImpactSpawner.Instance, WoodIstance.transform));
                        break;
                    default: break;
                }

                GameObject lineRenderer = BulletTracerSpawner.Instance.Spawn(bulletTracer[0].gameObject, r.origin, Quaternion.identity, BulletTracerSpawner.Instance.holder);
                lineRenderer.GetComponent<LineRenderer>().SetPosition(0, tracerTransform.position);
                lineRenderer.GetComponent<LineRenderer>().SetPosition(1, hit.point);
                lineRenderer.SetActive(true);

                SendDamage();
            }
        }
    }

    protected override void Reload()
    {
        if (Input.GetKeyDown(KeyCode.R) && CanReload()) HandleReload();
    }

    protected override void HandleReload()
    {
        StartCoroutine(SetReload());
    }

    protected void ShellEmit()
    {
        if (shellEmiter != null) shellEmiter.Emit(1);
    }

    protected override IEnumerator SetReload()
    {
        // Reload first
        animator.Play("Base Layer.Recharge_beginning", 0);
        yield return new WaitForSeconds(animationClips[0].length);

        // Reload
        while (weaponData.currentAmmo < weaponData.magazineSize - 1 && weaponData.ammo > 1)
        {
            if (!animator.GetCurrentAnimatorStateInfo(0).IsName("Shoot"))
            {
                animator.Play("Base Layer.Recharge", 0);
                yield return new WaitForSeconds(animationClips[1].length * 0.25f);
            }
            else
            {
                yield return new WaitForSeconds(weaponData.fireRate);
                animator.Play("Base Layer.Recharge_beginning", 0);
                yield return new WaitForSeconds(animationClips[0].length);
            }
        }

        // Reload last
        animator.Play("Base Layer.Recharge_End", 0);
        yield return new WaitForSeconds(animationClips[2].length * 0.75f);
    }

    protected void SetBullet()
    {
        weaponData.ammo -= 1;
        weaponData.currentAmmo += 1;
    }

    protected override void PlayReloadSound()
    {
        gunAudioSource.PlayOneShot(reloadSounds[0]);
    }

    protected void PlayZatvorSound()
    {
        gunAudioSource.PlayOneShot(zatvorSounds);
    }
}
