using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class M4A1 : Weapon
{
    [SerializeField] protected AudioClip drawSound;

    protected void OnEnable()
    {
        StartCoroutine(PlayDrawSound());
    }

    IEnumerator PlayDrawSound()
    {
        gunAudioSource.PlayOneShot(drawSound);
        yield return new WaitForSeconds(0.5f);
    }
}