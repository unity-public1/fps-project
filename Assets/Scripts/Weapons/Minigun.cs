using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minigun : Weapon
{
    [SerializeField] protected AudioSource servoAudioSource;
    [SerializeField] protected new Animation animation;
    [SerializeField] protected AnimationClip[] animationClips;
    [SerializeField] protected AudioClip[] readySounds;
    [SerializeField] protected AudioClip servoSound;
    [SerializeField] protected Transform barrel;

    protected float barrelRotateSpeed;
    protected float wantedSpeed = 0f;
    protected float wantedPitch = 0f;
    protected float pitchSpeed;

    protected void OnEnable()
    {
        OnEnableReady();
    }

    protected void OnDisable()
    {
        StopBarrel();
        StopServoSound();
    }

    protected void OnEnableReady()
    {
        animation.Stop();
        StartCoroutine(SetReady());
    }

    protected override void Fire()
    {
        if (weaponData.currentAmmo > 0)
        {
            if (Input.GetButton("Fire1") && CanFire())
            {
                SetBarrelSpeedUp();

                if (wantedSpeed > 500f)
                {
                    HandleFire();
                }
            }
            else if (Input.GetButtonUp("Fire1"))
            {
                SetBarrelSpeedDown();
                PlayAfterFireEffect();
            }

            if (Input.GetButton("Fire1") && CanAutomaticReload())
            {
                PlayReloadSound();
                HandleReload();
                SetBarrelSpeedDown();
                StopServoSound();
            }
        }
        else if (weaponData.currentAmmo == 0 && weaponData.ammo == 0)
        {
            SetBarrelSpeedDown();
        }

        if (Input.GetButtonDown("Fire1") && CanAutomaticReload())
        {
            PlayReloadSound();
            HandleReload();
            SetBarrelSpeedDown();
            StopServoSound();
        }

        if (Input.GetButtonDown("Fire1") && weaponData.currentAmmo <= 0)
        {
            PlayOutOfAmmoSound();
        }

        if (wantedSpeed >= 10f)
        {
            if (!servoAudioSource.isPlaying)
            {
                PlayServoSound();
            }
        }
        else
        {
            StopServoSound();
        }

        RotateBarrel();
    }

    protected override void HandleFire()
    {
        nextFire = Time.time + weaponData.fireRate;
        weaponData.currentAmmo--;

        animation[animationClips[1].name].speed = 2.5f;
        animation.Play(animationClips[1].name);

        PlayFireEffect();
        PlayFireSound();
        RecoilFire();
        CreateImpactEffect();
        CreateBulletTracer();
        EnableHitmarker();
        SendDamage();
        UpdateCrosshair();
    }

    protected override bool CanFire()
    {
        return !animation.IsPlaying(animationClips[0].name) && Time.time > nextFire;
    }

    protected override void Reload()
    {
        if (Input.GetKeyDown(KeyCode.R) && CanReload())
        {
            PlayReloadSound();
            SetBarrelSpeedDown();
            StopServoSound();
            animation.Play(animationClips[2].name);
        }
    }

    protected override void HandleReload()
    {
        animation.Play(animationClips[2].name);
    }

    protected override bool CanReload()
    {
        return !animation.IsPlaying(animationClips[0].name) && !animation.IsPlaying(animationClips[2].name) && weaponData.currentAmmo != weaponData.magazineSize && weaponData.ammo > 0;
    }

    protected override bool CanAutomaticReload()
    {
        return weaponData.currentAmmo <= 0 && weaponData.ammo > 0 && weaponData.reloadMode == ReloadMode.Automatic && !animation.IsPlaying(animationClips[0].name) && !animation.IsPlaying(animationClips[2].name);
    }

    protected override void EnableCrossHair()
    {
        if (!FirstPersonController.Instance.isWalking && !animation.IsPlaying(animationClips[1].name))
            crosshair.GetComponent<Crosshair>().UpdateSize(2);
    }

    protected void RotateBarrel()
    {
        wantedPitch = Mathf.Lerp(wantedPitch, pitchSpeed, Time.deltaTime * 2f);
        wantedSpeed = Mathf.Lerp(wantedSpeed, barrelRotateSpeed, Time.deltaTime * 2f);
        servoAudioSource.pitch = wantedPitch;
        barrel.Rotate(Vector3.forward * Time.deltaTime * wantedSpeed);
    }

    protected void StopBarrel()
    {
        wantedSpeed = 0f;
        wantedPitch = 0f;
    }

    protected void SetBarrelSpeedUp()
    {
        barrelRotateSpeed = 600f;
        pitchSpeed = 1.0f;
    }

    protected void SetBarrelSpeedDown()
    {
        barrelRotateSpeed = 0f;
        pitchSpeed = 0.3f;
    }

    protected void PlayServoSound()
    {
        servoAudioSource.clip = servoSound;
        servoAudioSource.loop = true;
        servoAudioSource.Play();
    }

    protected void StopServoSound()
    {
        servoAudioSource.Stop();
    }

    protected void SetAmmo()
    {
        int current;
        int result;

        current = weaponData.magazineSize - weaponData.currentAmmo;
        result = weaponData.ammo >= current ? current : weaponData.ammo;

        weaponData.currentAmmo += result;
        weaponData.ammo -= result;
    }

    protected virtual IEnumerator SetReady()
    {
        animation[animationClips[0].name].speed = 1.25f;
        animation.Play(animationClips[0].name);
        yield return new WaitForSeconds(animationClips[0].length);
    }
}