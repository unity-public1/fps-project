using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public abstract class Weapon : MonoBehaviour
{
    [Header("Component")]
    [SerializeField] protected Animator animator;
    [SerializeField] protected AudioSource playerAudioSource;
    [SerializeField] protected AudioSource gunAudioSource;
    [SerializeField] protected AudioSource hitAudioSource;
    [SerializeField] protected new Camera camera;
    [SerializeField] protected GameObject crosshair;
    [SerializeField] protected Hitmarker hitmarker;
    [SerializeField] protected Transform recoilTransform;
    [SerializeField] protected Transform tracerTransform;
    [SerializeField] protected TMP_Text ammoText;
    [SerializeField] public WeaponData weaponData;
    [SerializeField] protected bool shotgun;

    protected float nextFire = 0f;
    protected int damage => Random.Range(weaponData.minDamage, weaponData.maxDamage);
    protected int criticalDamage => damage * 2;  
    protected bool isAiming;
    protected float smoothTime = 0.03f;
    protected Vector3 velocity = Vector3.zero;
    protected Vector3 v = Vector3.zero;
    protected Vector3 currentRotation;
    protected Vector3 targetRotation;

    [Header("Effect")]
    [SerializeField] protected ImpactInfo[] ImpactElemets = new ImpactInfo[0];
    [SerializeField] protected ParticleGroupEmitter[] shotEmitters;
    [SerializeField] protected ParticleGroupPlayer[] afterFireSmoke;
    [SerializeField] protected LineRenderer[] bulletTracer;

    [Header("Sounds")]
    [SerializeField] protected AudioClip[] fireSounds;
    [SerializeField] protected AudioClip[] reloadSounds;
    [SerializeField] protected AudioClip[] hitSounds;

    [Header("Animations")]
    [SerializeField] protected AnimationClip reloadAnimation;

    public RaycastHit hit;
    public RaycastHit UIhit;

    protected void Awake()
    {
        InitAmmo();
    }

    protected void InitAmmo()
    {
        weaponData.ammo = weaponData.defaultAmmo;
        weaponData.currentAmmo = weaponData.defaultCurrentAmmo;
    }

    protected virtual void Update()
    {
        Fire();
        Aim();
        Recoil();
        Reload();
        EnableCrossHair();
        EnableInteractUI();
        SetAmmoText();
    }

    protected virtual void Fire()
    {
        if (weaponData.currentAmmo > 0)
        {
            if (weaponData.fireMode == FireMode.Automatic)
            {
                if (Input.GetButton("Fire1") && CanFire())
                {
                    HandleFire();
                }

                if (Input.GetButton("Fire1") && CanAutomaticReload())
                {
                    HandleReload();
                    PlayReloadSound();
                }

                if (Input.GetButtonUp("Fire1"))
                    PlayAfterFireEffect();
            }
            else if (weaponData.fireMode == FireMode.Single)
            {
                if (Input.GetButtonDown("Fire1") && CanFire())
                {
                    HandleFire();
                    PlayAfterFireEffect();
                }

                if (Input.GetButtonUp("Fire1"))
                    PlayAfterFireEffect();
            }
        }

        if (Input.GetButtonDown("Fire1") && CanAutomaticReload())
        {
            PlayReloadSound();
            HandleReload();
        }
        else if (Input.GetButtonDown("Fire1") && weaponData.currentAmmo <= 0)
        {
            PlayOutOfAmmoSound();
        }
    }

    protected virtual void HandleFire()
    {
        nextFire = Time.time + weaponData.fireRate;
        weaponData.currentAmmo--;
        animator.SetTrigger("Shoot");

        CreateImpactEffect();
        CreateBulletTracer();
        EnableHitmarker();
        UpdateCrosshair();
        PlayFireEffect();
        PlayFireSound();
        RecoilFire();
        SendDamage();
    }

    protected virtual bool CanFire()
    {
        return animator.GetCurrentAnimatorStateInfo(0).IsName("Idle") && Time.time > nextFire;
    }

    protected virtual void Reload()
    {
        if (Input.GetKeyDown(KeyCode.R) && CanReload())
        {
            PlayReloadSound();
            HandleReload();
        }
    }

    protected virtual void HandleReload()
    {
        animator.SetTrigger("Reload");
        StartCoroutine(SetReload());
    }

    protected virtual bool CanReload()
    {
        return animator.GetCurrentAnimatorStateInfo(0).IsName("Idle") && weaponData.currentAmmo != weaponData.magazineSize && weaponData.ammo > 0;
    }

    protected virtual bool CanAutomaticReload()
    {
        return weaponData.currentAmmo <= 0 && weaponData.ammo > 0 && weaponData.reloadMode == ReloadMode.Automatic && animator.GetCurrentAnimatorStateInfo(0).IsName("Idle");
    }

    protected virtual IEnumerator SetReload()
    {
        yield return new WaitForSeconds(reloadAnimation.length);

        int current;
        int result;

        current = weaponData.magazineSize - weaponData.currentAmmo;
        result = weaponData.ammo >= current ? current : weaponData.ammo;

        weaponData.currentAmmo += result;
        weaponData.ammo -= result;
    }

    protected void Recoil()
    {
        targetRotation = Vector3.Lerp(targetRotation, Vector3.zero, weaponData.returnSpeed * Time.deltaTime);
        currentRotation = Vector3.Slerp(currentRotation, targetRotation, weaponData.snappiness * Time.fixedDeltaTime);
        recoilTransform.localRotation = Quaternion.Euler(currentRotation);
    }

    protected virtual void Aim()
    {
        if (shotgun) return;

        if (Input.GetButton("Fire2"))
        {
            camera.fieldOfView = 50f;
            transform.localPosition = Vector3.SmoothDamp(transform.localPosition, weaponData.aimPosition, ref velocity, smoothTime);
            isAiming = true;
        }
        else
        {
            camera.fieldOfView = 60f;
            transform.localPosition = Vector3.SmoothDamp(transform.localPosition, weaponData.normalPosition, ref velocity, smoothTime);
            isAiming = false;
        }
    }

    protected virtual void EnableCrossHair()
    {
        if (isAiming || animator.GetCurrentAnimatorStateInfo(0).IsName("Reload"))
            crosshair.SetActive(false);
        else
            crosshair.SetActive(true);

        if (!FirstPersonController.Instance.isWalking && !animator.GetCurrentAnimatorStateInfo(0).IsName("Shoot"))
            UpdateCrosshair();
    }

    protected void UpdateCrosshair()
    {
        crosshair.GetComponent<Crosshair>().UpdateSize(2);
    }

    protected void EnableHitmarker()
    {
        switch (hit.transform.gameObject.layer)
        {
            case 6:
                hitmarker.Enable();
                hit.transform.GetComponent<ShootingSheet>().Enable();
                hitAudioSource.PlayOneShot(Resources.Load("Audio/Bullet/Hitmarker") as AudioClip);
                break;
            case 7:
                hitmarker.Enable(true);
                hitAudioSource.PlayOneShot(hitSounds[Random.Range(0, hitSounds.Length)]);
                break;
        }
    }

    protected void RecoilFire()
    {
        if (isAiming)
            targetRotation += new Vector3(weaponData.aimRecoil.x, Random.Range(-weaponData.aimRecoil.y, weaponData.aimRecoil.y), Random.Range(-weaponData.aimRecoil.z, weaponData.aimRecoil.z));
        else if (!FirstPersonController.Instance.isWalking && !isAiming)
            targetRotation += new Vector3(weaponData.runningRecoil.x, Random.Range(-weaponData.runningRecoil.y, weaponData.runningRecoil.y), Random.Range(-weaponData.runningRecoil.z, weaponData.runningRecoil.z));
        else
            targetRotation += new Vector3(weaponData.normalRecoil.x, Random.Range(-weaponData.normalRecoil.y, weaponData.normalRecoil.y), Random.Range(-weaponData.normalRecoil.z, weaponData.normalRecoil.z));
    }

    protected virtual void SendDamage()
    {
        switch (hit.transform.gameObject.layer)
        {
            case 19:
                hit.transform.GetComponent<BodyCollider>().enemyHealth.TakeDamage(damage); SpawnNormalDamageText(hit.transform, damage); break;    
            case 20:
                hit.transform.GetComponent<HeadCollider>().enemyHealth.TakeDamage(criticalDamage); SpawnCriticalDamageText(hit.transform, criticalDamage); break;
            default: 
                break;
        }
    }

    protected void SpawnNormalDamageText(Transform transform, float damage)
    {
        float YOffSet = Random.Range(0f, 1f);
        float XOffSet = Random.Range(-1f, 1f);
        Vector3 dir = new Vector3(1 * XOffSet, 1 * YOffSet, 0);

        GameObject damageText = FloatingDamageSpawner.Instance.Spawn(transform.position + dir, Quaternion.identity, FloatingDamageSpawner.Instance.holder);
        damageText.transform.position = transform.position + dir;
        damageText.GetComponent<FloatingDamage>().text.SetText(damage.ToString());
        damageText.SetActive(true);
    }

    protected void SpawnCriticalDamageText(Transform transform, float damage)
    {
        float YOffSet = Random.Range(0f, 1f);
        float XOffSet = Random.Range(-1f, 1f);
        Vector3 dir = new Vector3(1 * XOffSet, 1 * YOffSet, 0);

        GameObject damageText = FloatingDamageSpawner.Instance.Spawn(transform.position + dir, Quaternion.identity, FloatingDamageSpawner.Instance.holder);
        damageText.transform.position = transform.position + dir;
        damageText.GetComponent<FloatingDamage>().text.SetText(damage.ToString());
        damageText.GetComponent<FloatingDamage>().critical = true;
        damageText.SetActive(true);
    }

    protected virtual void CreateImpactEffect()
    {
        Ray ray = new Ray(recoilTransform.position, transform.forward);
        if (Physics.Raycast(ray, out hit, weaponData.bulletDistance))
        {
            GameObject effect = GetImpactEffect(hit.transform.gameObject);
            if (effect == null) return;

            switch (effect.name)
            {
                case "BrickImpact":     GameObject BrickIstance = GetSpawner("BrickImpact");
                                        InitImpact(BrickIstance);
                                        StartCoroutine(ImpactDespawn(5f, BrickImpactSpawner.Instance, BrickIstance.transform));
                                        break;
                case "ConcreteImpact":  GameObject ConcreteIstance = GetSpawner("ConcreteImpact");
                                        InitImpact(ConcreteIstance); 
                                        StartCoroutine(ImpactDespawn(5f, ConcreteImpactSpawner.Instance, ConcreteIstance.transform));
                                        break;   
                case "DirtImpact":      GameObject DirtIstance = GetSpawner("DirtImpact");
                                        InitImpact(DirtIstance); 
                                        StartCoroutine(ImpactDespawn(5f, DirtImpactSpawner.Instance, DirtIstance.transform));
                                        break;
                case "FoliageImpact":   GameObject FoliageIstance = GetSpawner("FoliageImpact");
                                        InitImpact(FoliageIstance); 
                                        StartCoroutine(ImpactDespawn(5f, FoliageImpactSpawner.Instance, FoliageIstance.transform));
                                        break;
                case "GlassImpact":     GameObject GlassIstance = GetSpawner("GlassImpact");
                                        InitImpact(GlassIstance); 
                                        StartCoroutine(ImpactDespawn(5f, GlassImpactSpawner.Instance, GlassIstance.transform));
                                        break; 
                case "MetalImpact":     GameObject MetalIstance = GetSpawner("MetalImpact");
                                        InitImpact(MetalIstance); 
                                        StartCoroutine(ImpactDespawn(5f, MetalImpactSpawner.Instance, MetalIstance.transform));
                                        break; 
                case "PlasterImpact":   GameObject PlasterIstance = GetSpawner("PlasterImpact");
                                        InitImpact(PlasterIstance); 
                                        StartCoroutine(ImpactDespawn(5f, PlasterImpactSpawner.Instance, PlasterIstance.transform));
                                        break; 
                case "RockImpact":      GameObject RockIstance = GetSpawner("RockImpact");
                                        InitImpact(RockIstance); 
                                        StartCoroutine(ImpactDespawn(5f, RockImpactSpawner.Instance, RockIstance.transform));
                                        break;
                case "SkinImpact":      GameObject SkinIstance = GetSpawner("SkinImpact");
                                        InitImpact(SkinIstance);
                                        StartCoroutine(ImpactDespawn(5f, SkinImpactSpawner.Instance, SkinIstance.transform));
                                        break;
                case "WaterImpact":     GameObject WaterIstance = GetSpawner("WaterImpact");
                                        InitImpact(WaterIstance);
                                        StartCoroutine(ImpactDespawn(5f, WaterImpactSpawner.Instance, WaterIstance.transform));
                                        break;
                case "WoodImpact":      GameObject WoodIstance = GetSpawner("WoodImpact");
                                        InitImpact(WoodIstance); 
                                        StartCoroutine(ImpactDespawn(5f, WoodImpactSpawner.Instance, WoodIstance.transform));
                                        break;
                default: break;
            }
        }
    }

    protected virtual void CreateBulletTracer()
    {
        if (shotgun) return;

        Ray ray = new Ray(tracerTransform.transform.position, transform.forward);
        if (Physics.Raycast(ray, out hit))
        {
            GameObject lineRenderer = BulletTracerSpawner.Instance.Spawn(bulletTracer[0].gameObject, ray.origin, Quaternion.identity, BulletTracerSpawner.Instance.holder);
            lineRenderer.GetComponent<LineRenderer>().SetPosition(0, tracerTransform.position);
            lineRenderer.GetComponent<LineRenderer>().SetPosition(1, hit.point);
            lineRenderer.SetActive(true);
        }
    }

    protected GameObject GetSpawner(string name)
    {
        switch (name)
        {
            case "BrickImpact":     return BrickImpactSpawner.Instance.Spawn(GetImpactEffect(hit.transform.gameObject), BrickImpactSpawner.Instance.holder);
            case "ConcreteImpact":  return ConcreteImpactSpawner.Instance.Spawn(GetImpactEffect(hit.transform.gameObject), ConcreteImpactSpawner.Instance.holder);
            case "DirtImpact":      return DirtImpactSpawner.Instance.Spawn(GetImpactEffect(hit.transform.gameObject), DirtImpactSpawner.Instance.holder);
            case "FoliageImpact":   return FoliageImpactSpawner.Instance.Spawn(GetImpactEffect(hit.transform.gameObject), FoliageImpactSpawner.Instance.holder);
            case "GlassImpact":     return GlassImpactSpawner.Instance.Spawn(GetImpactEffect(hit.transform.gameObject), GlassImpactSpawner.Instance.holder);
            case "MetalImpact":     return MetalImpactSpawner.Instance.Spawn(GetImpactEffect(hit.transform.gameObject), MetalImpactSpawner.Instance.holder);
            case "PlasterImpact":   return PlasterImpactSpawner.Instance.Spawn(GetImpactEffect(hit.transform.gameObject), PlasterImpactSpawner.Instance.holder);
            case "RockImpact":      return RockImpactSpawner.Instance.Spawn(GetImpactEffect(hit.transform.gameObject), RockImpactSpawner.Instance.holder);
            case "SkinImpact":      return SkinImpactSpawner.Instance.Spawn(GetImpactEffect(hit.transform.gameObject), SkinImpactSpawner.Instance.holder);
            case "WaterImpact":     return WaterImpactSpawner.Instance.Spawn(GetImpactEffect(hit.transform.gameObject), WaterImpactSpawner.Instance.holder);
            case "WoodImpact":      return WoodImpactSpawner.Instance.Spawn(GetImpactEffect(hit.transform.gameObject), WoodImpactSpawner.Instance.holder);
            default: return null;
        }
    }

    protected void InitImpact(GameObject impact)
    {
        impact.transform.position = hit.point;
        impact.transform.rotation = Quaternion.identity;
        impact.transform.LookAt(hit.point + hit.normal);
        impact.SetActive(true);
    }

    protected void PlayFireEffect()
    {
        if (afterFireSmoke != null)
        {
            foreach (ParticleGroupPlayer effect in afterFireSmoke)
                effect.Stop();
        }

        if (shotEmitters != null)
        {
            foreach (ParticleGroupEmitter effect in shotEmitters)
                effect.Emit(1);
        }
    }

    protected void PlayAfterFireEffect()
    {
        if (afterFireSmoke != null)
        {
            foreach (ParticleGroupPlayer effect in afterFireSmoke)
                effect.Play();
        }
    }

    protected void PlayFireSound()
    {
        gunAudioSource.Stop();
        playerAudioSource.PlayOneShot(fireSounds[Random.Range(0, fireSounds.Length)]);
        StartCoroutine(ShellDropSound());
    }   
    
    protected virtual void PlayReloadSound()
    {
        gunAudioSource.PlayOneShot(reloadSounds[Random.Range(0, reloadSounds.Length)]);
    }

    protected void PlayOutOfAmmoSound()
    {
        gunAudioSource.PlayOneShot(Resources.Load("Audio/Weapon/OutOfAmmoSound") as AudioClip);
    }

    protected IEnumerator ShellDropSound()
    {
        yield return new WaitForSeconds(0.5f);
        gunAudioSource.PlayOneShot(Resources.Load("Audio/Bullet/BulletDrop") as AudioClip);
    } 

    protected IEnumerator ImpactDespawn(float time, Spawner spawner, Transform obj)
    {
        yield return new WaitForSeconds(time);

        switch (spawner)
        {
            case BrickImpactSpawner: BrickImpactSpawner.Instance.Despawn(obj); break;
            case ConcreteImpactSpawner: ConcreteImpactSpawner.Instance.Despawn(obj); break;
            case DirtImpactSpawner: DirtImpactSpawner.Instance.Despawn(obj); break;
            case FoliageImpactSpawner: FoliageImpactSpawner.Instance.Despawn(obj); break;
            case GlassImpactSpawner: GlassImpactSpawner.Instance.Despawn(obj); break;
            case MetalImpactSpawner: MetalImpactSpawner.Instance.Despawn(obj); break;
            case PlasterImpactSpawner: PlasterImpactSpawner.Instance.Despawn(obj); break;
            case RockImpactSpawner: RockImpactSpawner.Instance.Despawn(obj); break;
            case SkinImpactSpawner: SkinImpactSpawner.Instance.Despawn(obj); break;
            case WaterImpactSpawner: WaterImpactSpawner.Instance.Despawn(obj); break;
            case WoodImpactSpawner: WoodImpactSpawner.Instance.Despawn(obj); break;
            default : break;
        }
    }

    protected GameObject GetImpactEffect(GameObject impactedGameObject)
    {
        MaterialType materialType = impactedGameObject.GetComponent<MaterialType>();
        if (materialType == null)
            return null;
        foreach (ImpactInfo impactInfo in ImpactElemets)
        {
            if (impactInfo.MaterialType == materialType.TypeOfMaterial)
                return impactInfo.ImpactEffect;
        }
        return null;
    }

    protected void SetAmmoText()
    {
        ammoText.text = weaponData.currentAmmo.ToString() + " / " + weaponData.ammo.ToString();
    }

    protected void EnableInteractUI()
    {
        Ray ray = new Ray(recoilTransform.position, transform.forward);
        if (Physics.Raycast(ray, out UIhit, 4))
        {
            if (UIhit.transform.gameObject.CompareTag("Item"))
            {
                InteractUI.Instance.holder.SetActive(true);
                InteractUI.Instance.interactText.text = "Take " + UIhit.transform.GetComponent<Test>().itemName;
            }
        }
        else 
            InteractUI.Instance.holder.SetActive(false);
    }
}