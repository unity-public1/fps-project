using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Syringe : MonoBehaviour
{
    [Header("Component")]
    [SerializeField] protected AudioSource playerAudioSource;
    [SerializeField] protected AudioClip injectSound;
    [SerializeField] protected TMP_Text syringeAmountText;

    protected void Update()
    {
        SetAmmoText();
    }

    protected void InjectSyringe()
    {
        WeaponSelect.Instance.syringeAmount--;
        PlayerHealth.Instance.Heal(100f);
        CameraBlur.Instance.radius = 8.0f;
        HealIndicator.Instance.Show();
        playerAudioSource.PlayOneShot(injectSound);
    }

    protected void SetAmmoText()
    {
        syringeAmountText.text = WeaponSelect.Instance.syringeAmount.ToString();
    }
}
