using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketExplotion : MonoBehaviour
{
    [SerializeField] protected AudioClip[] explodeSounds;
    [SerializeField] protected Vector3 myRotation;
    [SerializeField] protected AudioSource audioSource;

    protected void OnEnable()
    {
        RocketExplosionSpawner.Instance.DespawnAfterTime(gameObject.transform, 2f);

        transform.rotation = Quaternion.Euler(myRotation);
        int ran = Random.Range(1, explodeSounds.Length);
        audioSource.clip = explodeSounds[ran];
        audioSource.pitch = 0.9f + 0.1f * Random.value;
        audioSource.PlayOneShot(audioSource.clip);

        explodeSounds[ran] = explodeSounds[0];
        explodeSounds[0] = audioSource.clip;
    }

    protected void Start()
    {
        RocketExplosionSpawner.Instance.DespawnAfterTime(gameObject.transform, 2f);

        transform.rotation = Quaternion.Euler(myRotation);
        int ran = Random.Range(1, explodeSounds.Length);
        audioSource.clip = explodeSounds[ran];
        audioSource.pitch = 0.9f + 0.1f * Random.value;
        audioSource.PlayOneShot(audioSource.clip);

        explodeSounds[ran] = explodeSounds[0];
        explodeSounds[0] = audioSource.clip;
    }
}
