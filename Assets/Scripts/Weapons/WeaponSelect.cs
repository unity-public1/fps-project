using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSelect : MonoBehaviour
{
    protected static WeaponSelect instance;
    public static WeaponSelect Instance => instance;

    [SerializeField] public int syringeAmount = 3;
    [SerializeField] protected Weapon weapon = Weapon.M4A1;
    [SerializeField] public GameObject[] weaponsObject;

    protected enum Weapon { DE, M4A1, Shotgun, HMG, Slugger, Minigun, Grenade, Syringe }

    protected void Awake() => CreateSingleton();


    protected void Update()
    {
        SelectWeapon();
    }

    protected void SelectWeapon()
    {
        if (Input.GetKey(KeyCode.Alpha1))
            weapon = Weapon.DE;
        else if (Input.GetKey(KeyCode.Alpha2))
            weapon = Weapon.M4A1;
        else if (Input.GetKey(KeyCode.Alpha3))
            weapon = Weapon.Shotgun; 
        else if (Input.GetKey(KeyCode.Alpha4))
            weapon = Weapon.HMG; 
        else if (Input.GetKey(KeyCode.Alpha5))
            weapon = Weapon.Slugger;
        else if (Input.GetKey(KeyCode.Alpha6))
            weapon = Weapon.Minigun;
        else if (Input.GetKey(KeyCode.G))
            weapon = Weapon.Grenade;
        else if (Input.GetKey(KeyCode.Z) && weapon != Weapon.Syringe && syringeAmount > 0 /*&& PlayerHealth.Instance.hP < 100*/)
            StartCoroutine(HealAndChangeToLastWeapon());
                
        for (int i = 0; i < weaponsObject.Length; i++)
        {
            if (i == (int) weapon) 
                weaponsObject[i].gameObject.SetActive(true);
            else 
                weaponsObject[i].gameObject.SetActive(false);
        }
    }

    protected IEnumerator HealAndChangeToLastWeapon()
    {
        Weapon lastWeapon = weapon;
        yield return new WaitForSeconds(0.2f);
        weapon = Weapon.Syringe;
        yield return new WaitForSeconds(2.355f);
        weapon = lastWeapon;
    }

    protected void CreateSingleton()
    {
        if (instance != null) return;
        instance = this;
    }
}
