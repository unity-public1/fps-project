using System.Collections;
using System.Collections.Generic;
using Unity.Burst.CompilerServices;
using Unity.VisualScripting;
using UnityEngine;

public class Grenade : MonoBehaviour
{
    [Header("Component")]
    [SerializeField] protected new Rigidbody rigidbody;
    [SerializeField] protected Collider grenadeCollider;
    [SerializeField] protected GameObject explosionPrefab;

    [Header("Attribute")]
    [SerializeField] protected float damage = 150f;
    [SerializeField] protected float speed = 5f;
    [SerializeField] protected Vector3 angularSpeed = new Vector3(60, 0, 0);
    [SerializeField] public float explodeTime = 3f;
    [SerializeField] protected float explosionRadius = 10f;
    [SerializeField] protected float throwForce = 30f;

    protected float elapsedTime = 0;
    protected bool isExploded;

    protected void OnEnable()
    {
        Respawn();
    }

    protected void Respawn()
    {
        isExploded = false;
        grenadeCollider.enabled = true;
        elapsedTime = 0;
    }

    protected void Update()
    {
        elapsedTime += Time.deltaTime;

        if (elapsedTime >= explodeTime)
        {
            Explode();
            CameraShake.Instance.SetShakeCamera(0.4f, 3);
            GrenadeSpawner.Instance.Despawn(gameObject.transform);
        }
    }

    protected void Explode()
    {
        grenadeCollider.enabled = false;

        if (isExploded) return;

        if (explosionPrefab != null)
        {
            GameObject newExplosion = ExplosionSpawner.Instance.Spawn(explosionPrefab, transform.position, Quaternion.identity, ExplosionSpawner.Instance.holder);

            newExplosion.SetActive(true);
            newExplosion.transform.position = transform.position;
            newExplosion.transform.rotation = Quaternion.identity;

            CollisionPlacer[] autoPlacers = newExplosion.GetComponentsInChildren<CollisionPlacer>();
            foreach (CollisionPlacer ap in autoPlacers)
            {
                ap.AutoPlace();
            }

            ExplosionSpawner.Instance.DespawnAfterTime(newExplosion.transform, 10f);
        }

        Collider[] nearColliders = Physics.OverlapSphere(transform.position, explosionRadius * transform.lossyScale.magnitude);

        foreach (Collider collider in nearColliders)
        {
            SendDamage(collider);
        }

        isExploded = true;
    }

    protected void SendDamage(Collider collider)
    {
        switch (collider.transform.gameObject.layer)
        {
            case 19:
                collider.transform.GetComponent<BodyCollider>().enemyHealth.TakeDamage(damage);
                SpawnDamageText(collider.transform);
                break;
            case 8:
                if (collider.transform.GetComponent<PlayerHealth>().armor > 0)
                    collider.transform.GetComponent<PlayerHealth>().ArmorConsume(damage / 2);
                else
                {
                    collider.transform.GetComponent<PlayerHealth>().TakeDamage(damage / 2);
                    EnemyWaveController.Instance.damageTaken += damage/2;
                }
                break;
        }
    }

    protected void SpawnDamageText(Transform transform)
    {
        float YOffSet = Random.Range(0f, 1f);
        float XOffSet = Random.Range(-1f, 1f);
        Vector3 dir = new Vector3(1 * XOffSet, 1 * YOffSet, 0);

        GameObject damageText = FloatingDamageSpawner.Instance.Spawn(transform.position + dir, Quaternion.identity, FloatingDamageSpawner.Instance.holder);
        damageText.transform.position = transform.position + dir;
        damageText.GetComponent<FloatingDamage>().text.SetText(damage.ToString());
        damageText.SetActive(true);
    }
}