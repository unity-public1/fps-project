using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rocket : MonoBehaviour
{
    [SerializeField] new Rigidbody rigidbody;
    [SerializeField] protected float speed = 100f;
    [SerializeField] protected GameObject explosion;
    [SerializeField] protected float damage;
    [SerializeField] protected float radius;

    protected void Update()
    {
        rigidbody.AddRelativeForce(0f, 0f, speed);
    }

    protected void OnCollisionEnter()
    {
        GameObject newRocketExplosion = RocketExplosionSpawner.Instance.Spawn(RocketExplosionSpawner.Instance.holder);
        newRocketExplosion.SetActive(true);
        newRocketExplosion.transform.position = transform.position;
        newRocketExplosion.transform.rotation = transform.rotation;
        RocketSpawner.Instance.Despawn(gameObject.transform);

        Vector3 explosionPos = transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
        foreach (Collider hit in colliders)
        {
            if (hit.transform.gameObject.layer == 19) 
            { 
                 hit.transform.GetComponent<BodyCollider>().enemyHealth.TakeDamage(damage);
                 SpawnDamageText(hit.transform);
            }
        }
    }

    protected void SpawnDamageText(Transform transform)
    {
        float YOffSet = Random.Range(0f, 1f);
        float XOffSet = Random.Range(-1f, 1f);
        Vector3 dir = new Vector3(1 * XOffSet, 1 * YOffSet, 0);

        GameObject damageText = FloatingDamageSpawner.Instance.Spawn(transform.position + dir, Quaternion.identity, FloatingDamageSpawner.Instance.holder);
        damageText.transform.position = transform.position + dir;
        damageText.GetComponent<FloatingDamage>().text.SetText(damage.ToString());
        damageText.SetActive(true);
    }
}
