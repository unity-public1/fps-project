using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InsectSpawner : Spawner
{
    protected static InsectSpawner instance;
    public static InsectSpawner Instance => instance;

    [SerializeField] public int enemyAmount;
    [SerializeField] public Transform holder;

    protected void Awake() => CreateSingleton();

    public void SpawnEnemy()
    {
        if (spawnCount >= enemyAmount) return;
        GameObject enemy = Spawn(holder);

        if (enemy == null) return;
        enemy.transform.position = transform.position;
        enemy.gameObject.SetActive(true);
    }

    public void SpawnEnemyAtPosition(Transform spawnPos)
    {
        GameObject enemy = Spawn(spawnPos);

        if (enemy == null) return;
        enemy.transform.position = spawnPos.position;
        enemy.gameObject.SetActive(true);
    }

    protected void CreateSingleton()
    {
        if (instance != null) return;
        instance = this;
    }
}