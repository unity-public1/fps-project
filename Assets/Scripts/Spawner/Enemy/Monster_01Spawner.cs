using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster_01Spawner : Spawner
{
    protected static Monster_01Spawner instance;
    public static Monster_01Spawner Instance => instance;

    [SerializeField] public int enemyAmount;
    [SerializeField] public Transform holder;

    protected void Awake() => CreateSingleton();

    //protected void Update()
    //{
    //    SpawnEnemy();
    //}

    public void SpawnEnemy()
    {
        if (spawnCount >= enemyAmount) return;
        GameObject enemy = Spawn(holder);

        if (enemy == null) return;
        enemy.transform.position = transform.position;
        enemy.gameObject.SetActive(true);
    }

    public void SpawnEnemyAtPosition(Transform spawnPos)
    {
        GameObject enemy = Spawn(spawnPos);

        if (enemy == null) return;
        enemy.transform.position = spawnPos.position;
        enemy.gameObject.SetActive(true);
    }

    protected void CreateSingleton()
    {
        if (instance != null) return;
        instance = this;
    }
}
