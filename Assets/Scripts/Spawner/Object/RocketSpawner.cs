using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketSpawner : Spawner
{
    protected static RocketSpawner instance;
    public static RocketSpawner Instance => instance;

    [SerializeField] public Transform holder;

    protected void Awake() => CreateSingleton();

    protected void CreateSingleton()
    {
        if (instance != null) return;
        instance = this;
    }
}
