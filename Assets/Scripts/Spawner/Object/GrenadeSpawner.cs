using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeSpawner : Spawner
{
    protected static GrenadeSpawner instance;
    public static GrenadeSpawner Instance => instance;

    [SerializeField] public Transform holder;

    protected void Awake() => CreateSingleton();

    protected void CreateSingleton()
    {
        if (instance != null) return;
        instance = this;
    }
}
