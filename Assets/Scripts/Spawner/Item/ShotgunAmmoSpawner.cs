using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotgunAmmoSpawner : Spawner
{
    protected static ShotgunAmmoSpawner instance;
    public static ShotgunAmmoSpawner Instance => instance;

    [SerializeField] public Transform holder;

    protected void Awake() => CreateSingleton();

    protected void CreateSingleton()
    {
        if (instance != null) return;
        instance = this;
    }
}
