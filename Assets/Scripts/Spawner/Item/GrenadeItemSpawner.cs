using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeItemSpawner : Spawner
{
    protected static GrenadeItemSpawner instance;
    public static GrenadeItemSpawner Instance => instance;

    [SerializeField] public Transform holder;

    protected void Awake() => CreateSingleton();

    protected void CreateSingleton()
    {
        if (instance != null) return;
        instance = this;
    }
}
