using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MGAmmoSpawner : Spawner
{
    protected static MGAmmoSpawner instance;
    public static MGAmmoSpawner Instance => instance;

    [SerializeField] public Transform holder;

    protected void Awake() => CreateSingleton();

    protected void CreateSingleton()
    {
        if (instance != null) return;
        instance = this;
    }
}
