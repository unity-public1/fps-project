using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RifleAmmoSpawner : Spawner
{
    protected static RifleAmmoSpawner instance;
    public static RifleAmmoSpawner Instance => instance;

    [SerializeField] public Transform holder;

    protected void Awake() => CreateSingleton();

    protected void CreateSingleton()
    {
        if (instance != null) return;
        instance = this;
    }
}
