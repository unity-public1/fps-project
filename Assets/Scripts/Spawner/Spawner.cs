using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Spawner : MonoBehaviour
{
    [SerializeField] public int spawnCount = 0;
    [SerializeField] protected GameObject prefab;
    [SerializeField] protected List<GameObject> pooledObjects = new List<GameObject>();

    public virtual GameObject Spawn()
    {
        GameObject newPrefab = GetObjectFromPool(prefab);
        spawnCount++;
        return newPrefab;
    }

    public virtual GameObject Spawn(Transform holder)
    {
        GameObject newPrefab = GetObjectFromPool(prefab, holder);
        spawnCount++;
        return newPrefab;
    }
    public virtual GameObject Spawn(GameObject prefab, Transform holder)
    {
        GameObject newPrefab = GetObjectFromPool(prefab, holder);
        spawnCount++;
        return newPrefab;
    }

    public virtual GameObject Spawn(Vector3 position, Quaternion rotation, Transform holder)
    {
        GameObject newPrefab = GetObjectFromPool(prefab, position, rotation, holder);
        spawnCount++;
        return newPrefab;
    }


    public virtual GameObject Spawn(GameObject prefab, Vector3 position, Quaternion rotation, Transform holder)
    {
        GameObject newPrefab = GetObjectFromPool(prefab, position, rotation , holder);

        spawnCount++;
        return newPrefab;
    }

    protected GameObject GetObjectFromPool(GameObject prefab)
    {
        foreach (GameObject pooledObject in pooledObjects)
        {
            if (pooledObject == null) continue;
            pooledObjects.Remove(pooledObject);
            return pooledObject;
        }

        GameObject newPrefab = Instantiate(prefab);
        return newPrefab;
    }

    protected GameObject GetObjectFromPool(GameObject prefab, Transform holder)
    {
        foreach (GameObject pooledObject in pooledObjects)
        {
            if (pooledObject == null) continue;
            pooledObjects.Remove(pooledObject);
            return pooledObject;
        }

        GameObject newPrefab = Instantiate(prefab, holder);
        return newPrefab;
    }

    protected GameObject GetObjectFromPool(GameObject prefab, Vector3 position, Quaternion rotation, Transform holder)
    {
        foreach (GameObject pooledObject in pooledObjects)
        {
            if (pooledObject == null) continue;
            pooledObjects.Remove(pooledObject);
            return pooledObject;
        }

        GameObject newPrefab = Instantiate(prefab, position, rotation, holder);
        return newPrefab;
    }

    public virtual void Despawn(Transform obj)
    {
        pooledObjects.Add(obj.gameObject);
        obj.gameObject.SetActive(false);
        spawnCount--;
    }

    public virtual void DespawnAfterTime(Transform obj, float time)
    {
        StartCoroutine(WaitToDespawn(obj, time));
    }

    public IEnumerator WaitToDespawn(Transform obj, float time)
    {
        yield return new WaitForSeconds(time);
        pooledObjects.Add(obj.gameObject);
        obj.gameObject.SetActive(false);
        spawnCount--;
    }
}
