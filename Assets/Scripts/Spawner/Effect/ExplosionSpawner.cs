using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionSpawner : Spawner
{
    protected static ExplosionSpawner instance;
    public static ExplosionSpawner Instance => instance;

    [SerializeField] public Transform holder;

    protected void Awake() => CreateSingleton();

    protected void CreateSingleton()
    {
        if (instance != null) return;
        instance = this;
    }
}
