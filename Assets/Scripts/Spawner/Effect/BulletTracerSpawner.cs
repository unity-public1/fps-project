using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletTracerSpawner : Spawner
{
    protected static BulletTracerSpawner instance;
    public static BulletTracerSpawner Instance => instance;

    [SerializeField] public Transform holder;

    protected void Awake() => CreateSingleton();

    protected void CreateSingleton()
    {
        if (instance != null) return;
        instance = this;
    }
}
