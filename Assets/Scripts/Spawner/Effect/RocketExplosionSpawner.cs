using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketExplosionSpawner : Spawner
{
    protected static RocketExplosionSpawner instance;
    public static RocketExplosionSpawner Instance => instance;

    [SerializeField] public Transform holder;

    protected void Awake() => CreateSingleton();

    protected void CreateSingleton()
    {
        if (instance != null) return;
        instance = this;
    }
}
