using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ASyncLoader : MonoBehaviour
{
    [SerializeField] protected GameObject loadingCanvas;
    [SerializeField] protected GameObject mainCanvas;
    [SerializeField] protected Slider loadingSlider;
    [SerializeField] protected GameObject startObject;
    [SerializeField] protected GameObject stageSelectObject;
    [SerializeField] protected CanvasGroup startGroup;
    [SerializeField] protected CanvasGroup stageSelectGroup;

    public void LoadScene(string levelName)
    {
        mainCanvas.SetActive(false);
        loadingCanvas.SetActive(true);

        StartCoroutine(LoadASync(levelName));
    }

    public void StageSelect()
    {
        StartCoroutine(EnableStageSelect());
    }

    public void BackToStart()
    {
        StartCoroutine(Back());
    }

    protected IEnumerator LoadASync(string levelName)
    {
        AsyncOperation loadOperation = SceneManager.LoadSceneAsync(levelName);

        while (!loadOperation.isDone)
        {
            float progressValue = Mathf.Clamp01(loadOperation.progress / 0.9f);
            loadingSlider.value = progressValue;
            Time.timeScale = 1;
            yield return null;
        }
    }

    protected IEnumerator EnableStageSelect()
    {
        stageSelectObject.SetActive(true);
        startGroup.interactable = false;
        while (stageSelectGroup.alpha != 1f)
        {
            yield return new WaitForSeconds(Time.deltaTime);
            stageSelectGroup.alpha += Time.deltaTime * 3f;
            startGroup.alpha -= Time.deltaTime * 3f;
        }
        stageSelectGroup.interactable = true;
        startObject.SetActive(false);
    }

    protected IEnumerator Back()
    {
        startObject.SetActive(true);
        stageSelectGroup.interactable = false;
        while (startGroup.alpha != 1f)
        {
            yield return new WaitForSeconds(Time.deltaTime);
            stageSelectGroup.alpha -= Time.deltaTime * 3f;
            startGroup.alpha += Time.deltaTime * 3f;
        }
        startGroup.interactable = true;
        stageSelectObject.SetActive(false);
    }
}