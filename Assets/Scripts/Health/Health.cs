using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Health : MonoBehaviour
{
    [Header("Parameters")]
    [SerializeField] public float hP = 100;
    [SerializeField] public float maxHP = 100;

    public bool isDead;

    protected void OnEnable()
    {
        Respawn();
    }

    protected virtual void Update()
    {
        CheckDead();
    }

    protected virtual void CheckDead()
    {
        if (hP <= 0)
        {
            isDead = true;
            hP = 0;
        }
    }

    public virtual void TakeDamage(float damage)
    {
        if (isDead) return;

        hP -= damage;

        if (hP <= 0)
        {
            isDead = true;
            hP = 0;
        }
    }

    public virtual void Respawn()
    {
        isDead = false;
    }

    public virtual void OnDead()
    {
        if (isDead == true)
        {
            transform.parent.gameObject.layer = 0;

            Destroy(transform.parent.gameObject, 3f);
        }
    }
}