using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : Health
{
    protected static PlayerHealth instance;
    public static PlayerHealth Instance => instance;

    [SerializeField] public float armor;
    [SerializeField] public float maxArmor;
    [SerializeField] public float stamina;
    [SerializeField] protected float maxStamina;
    [SerializeField] protected int staminaCost;
    [SerializeField] protected int staminaRecover;

    [Header("Component")]
    [SerializeField] public AudioSource playerAudioSource;
    [SerializeField] protected AudioClip heartbeatSound;
    [SerializeField] protected AudioClip deathSound;
    [SerializeField] public AudioClip[] gruntSounds;

    [HideInInspector] public float currentHP;
    protected int defaultHP = 100;

    protected void Awake() => CreateSingleton();

    protected override void Update()
    {
        base.Update();
        GetHit();
    }

    protected override void CheckDead()
    {
        if (hP <= 0)
        {
            if (!playerAudioSource.isPlaying) playerAudioSource.PlayOneShot(deathSound);
            isDead = true;
            hP = 0;
        }
    }

    protected void GetHit()
    {
        if (hP < currentHP)
        {
            playerAudioSource.PlayOneShot(heartbeatSound);
            playerAudioSource.PlayOneShot(gruntSounds[Random.Range(0, gruntSounds.Length)]);
            currentHP = hP;
        }
    }

    public void StaminaConsume()
    {
        if (stamina <= 0) return;
        stamina -= staminaCost * Time.deltaTime;
        if (stamina <= 0) stamina = 0;
    }

    public void StaminaRecover()
    {
        if (stamina >= maxStamina) return;
        stamina += staminaRecover * Time.deltaTime;
        if (stamina >= 100) stamina = maxStamina;
    }

    public void ArmorConsume(float amount)
    {
        if (armor <= 0) return;
        armor -= amount;
        if (armor <= 0) armor = 0;
    }

    public void ArmorRecover(float amount)
    {
        if (armor >= maxArmor) return;
        armor += amount;
        if (armor >= 100) armor = maxArmor;
    }

    public virtual void Heal(float amount)
    {
        if (isDead) return;
        hP += amount;
        if (hP >= 100) hP = maxHP;
    }

    protected void CreateSingleton()
    {
        if (instance != null) return;
        instance = this;
    }
}
