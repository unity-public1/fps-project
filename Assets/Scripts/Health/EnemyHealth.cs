using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : Health
{   
    [SerializeField] protected Animator animator;
    [SerializeField] protected EnemyAI monsterAI;
    [SerializeField] protected GameObject bodyCollider;
    [SerializeField] protected GameObject headCollider;
    [SerializeField] protected int defaultHP = 100;
    public float currentHP;

    protected void Awake()
    {
        currentHP = defaultHP;
    }

    protected override void Update()
    {   
        base.Update();
        OnDead();
    }

    public override void OnDead()
    {
        if (isDead == true)
        {
            // Set enemy layer to default
            transform.gameObject.layer = 0;
            if (bodyCollider != null) bodyCollider.layer = 0;
            if (headCollider != null) headCollider.layer = 0;
        }
    }

    public override void Respawn()
    {
        isDead = false;

        hP = defaultHP;

        currentHP = defaultHP;

        transform.gameObject.layer = 7;
        if (bodyCollider != null) bodyCollider.layer = 19;
        if (headCollider != null) headCollider.layer = 20;

        monsterAI.enabled = true;
    }

    protected virtual void DespawnAfterDeath()
    {
        // For override
    }
}
