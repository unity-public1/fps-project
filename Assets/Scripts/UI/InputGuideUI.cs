using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputGuideUI : MonoBehaviour
{
    public bool isON { get; set; }
    public KeyCode activationKey = KeyCode.F1;
    public List<GameObject> inputGuideObject;

    protected void Start()
    {
        SetActive(false);
    }

    protected void Update()
    {
        if (Input.GetKeyDown(activationKey))
        {
            isON = !isON;
            SetActive(isON);
        }
    }

    protected void SetActive(bool state)
    {
        isON = state;
        foreach (GameObject _object in inputGuideObject) _object.SetActive(isON);
    }
}
