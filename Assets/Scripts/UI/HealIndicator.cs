using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealIndicator : MonoBehaviour
{
    protected static HealIndicator instance;
    public static HealIndicator Instance => instance;

    public float alpha = 0.4f;
    public float fadeSpeed = 2;
    public CanvasGroup screenEffects;

    protected void Awake() => CreateSingleton();

    private void Update()
    {
        if (screenEffects.alpha != 0)
        {
            screenEffects.alpha = Mathf.Lerp(screenEffects.alpha, 0, Time.deltaTime * fadeSpeed);
        }
    }

    public void Show()
    {
        screenEffects.alpha = alpha;
    }

    public void Show(float alpha)
    {
        screenEffects.alpha = alpha;
    }

    protected void CreateSingleton()
    {
        if (instance != null) return;
        instance = this;
    }
}