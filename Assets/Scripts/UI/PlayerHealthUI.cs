using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthUI : HealthUI
{
    [SerializeField] protected PlayerHealth health;
    [SerializeField] protected Image staminaUI;
    [SerializeField] protected Image armorUI;

    protected void Update()
    {
        UpdateHPBar();
        UpdateStaminaBar();
        UpdateArmorBar();
    }

    protected void UpdateHPBar()
    {
        DisplayHP(health.hP);
    }

    protected void UpdateStaminaBar()
    {
        DisplayStamina(health.stamina);
    }

    protected void UpdateArmorBar()
    {
        DisplayArmor(health.armor);
    }

    public void DisplayStamina(float value)
    {
        value /= 100f;

        if (value <= 0) value = 0;
        staminaUI.fillAmount = value;
    }

    public void DisplayArmor(float value)
    {
        value /= 100f;

        if (value <= 0) value = 0;
        armorUI.fillAmount = value;
    }
}
