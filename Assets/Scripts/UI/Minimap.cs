using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minimap : MonoBehaviour
{
    [SerializeField] protected Transform player;
    [SerializeField] protected bool rotateWithPlayer;

    protected void LateUpdate()
    {
        MinimapFollowPlayer();
    }

    protected void MinimapFollowPlayer()
    {
        Vector3 newPosion = player.position;
        newPosion.y = transform.position.y;
        transform.position = newPosion;    
        
        if (rotateWithPlayer) 
            transform.rotation = Quaternion.Euler(90f, player.eulerAngles.y, 0f);
    }
}
