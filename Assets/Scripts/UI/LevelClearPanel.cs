using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LevelClearPanel : MonoBehaviour
{
    protected static LevelClearPanel instance;
    public static LevelClearPanel Instance => instance;

    [SerializeField] public GameObject resultHolder;
    [SerializeField] public GameObject afterResultHolder;
    [SerializeField] public GameObject stageSelect;
    [SerializeField] public GameObject player;
    [SerializeField] public GameObject playerUI;
    [SerializeField] public GameObject playerWeapon;
    [SerializeField] public CanvasGroup backGround;
    [SerializeField] public TMP_Text timeText;
    [SerializeField] public TMP_Text scoreText;
    [SerializeField] public TMP_Text killText;
    [SerializeField] public TMP_Text damageText;
    [HideInInspector] public bool resultShowed;
    [HideInInspector] public bool keyPressed;

    protected void Awake() => CreateSingleton();

    protected void Update()
    {
        if (Input.anyKey && resultShowed && !keyPressed)
        {
            keyPressed = true;
            resultHolder.SetActive(false);
            afterResultHolder.SetActive(true);
        }
    }

    protected void CreateSingleton()
    {
        if (instance != null) return;
        instance = this;
    }

    public void SetLevelClear()
    {
        Time.timeScale = 0;
        playerUI.SetActive(false);
        playerWeapon.SetActive(false);
        FirstPersonController.Instance.enabled = false;
        timeText.text = $"TIME  :  {TimeAndScoreUI.Instance.time}";
        scoreText.text = $"SCORE  :  {TimeAndScoreUI.Instance.scores}";
        killText.text = $"KILL  :  {EnemyWaveController.Instance.killCount}";
        damageText.text = $"DAMAGE TAKEN :  {EnemyWaveController.Instance.damageTaken}";
    }

    public void ResultBack()
    {
        keyPressed = false;
        resultHolder.SetActive(true);
        afterResultHolder.SetActive(false);
    }

    public void AfterResultBack()
    {
        afterResultHolder.SetActive(true);
        stageSelect.SetActive(false);
    }

    public void EnableStageSelect()
    {
        stageSelect.SetActive(true);
        afterResultHolder.SetActive(false);
    }
}
