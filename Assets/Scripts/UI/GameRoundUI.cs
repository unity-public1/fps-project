using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GameRoundUI : MonoBehaviour
{
    protected static GameRoundUI instance;
    public static GameRoundUI Instance => instance;

    [SerializeField] public GameObject roundStart;
    [SerializeField] public GameObject roundClear;
    [SerializeField] public TMP_Text gameRoundText;
    [SerializeField] public TMP_Text roundStartText;
    [SerializeField] public float timer;

    protected void Awake() => CreateSingleton();

    public void SetRoundStartText()
    {
        roundStartText.text = $"Enemies will appear after {(int)timer} seconds" ;
    }

    public void SetRoundText(string roundText)
    {
        gameRoundText.text = "ROUND   " + roundText.ToString()  + "  / " + EnemyWaveController.Instance.waves.ToString();
    }

    protected void CreateSingleton()
    {
        if (instance != null) return;
        instance = this;
    }
}
