using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class FloatingDamage : MonoBehaviour
{
    [SerializeField] public Animator animator;
    [SerializeField] public TMP_Text text;
    [HideInInspector] public bool critical { get; set; }
    [SerializeField] protected float despawnTime = 0.5f;

    protected Camera cam => Camera.main;
    protected Vector3 dir => cam.transform.position - transform.position;

    protected void OnEnable()
    {
        FloatingDamageSpawner.Instance.DespawnAfterTime(transform, despawnTime);

        if (critical)
            animator.SetTrigger("Critical");
        else
            animator.SetTrigger("Normal");
    }

    protected void OnDisable()
    {
        critical = false;
    }

    protected void LateUpdate()
    {
        transform.rotation = Quaternion.LookRotation(dir.normalized, cam.transform.up);
        transform.localRotation *= Quaternion.Euler(0, 180, 0);
    }
}