using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartButton : MonoBehaviour
{
    [SerializeField] protected Button startButton;

    public void TaskOnClick()
    {
        StartCoroutine(ButtonClicked());
    }

    public IEnumerator ButtonClicked()
    {
        yield return new WaitForSeconds(0.1f);
        SceneManager.LoadSceneAsync(1);
    }
}
