using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingDamageSpawner : Spawner
{
    protected static FloatingDamageSpawner instance;
    public static FloatingDamageSpawner Instance => instance;

    [SerializeField] public Transform holder;

    protected void Awake() => CreateSingleton();

    protected void CreateSingleton()
    {
        if (instance != null) return;
        instance = this;
    }
}
