using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PickUpUI : MonoBehaviour
{
    protected static PickUpUI instance;
    public static PickUpUI Instance => instance;

    [SerializeField] public GameObject holder;
    [SerializeField] public TMP_Text pickUpText;
    [SerializeField] public Image ammoImage;
    protected float delayTime = 1f;

    protected void Awake() => CreateSingleton();

    protected void CreateSingleton()
    {
        if (instance != null) return;
        instance = this;
    }

    public void DisablePickupUI()
    {
        StartCoroutine(DisableUIAfterTime(delayTime));
    }

    protected IEnumerator DisableUIAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
        holder.gameObject.SetActive(false);
    }
}
