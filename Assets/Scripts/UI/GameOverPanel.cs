using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverPanel : MonoBehaviour
{
    [SerializeField] PlayerHealth playerHealth;
    [SerializeField] FirstPersonController controller;
    [SerializeField] GameObject playerWeapons;
    [SerializeField] GameObject playerUI;
    [SerializeField] GameObject panel;
    [SerializeField] GameObject damageIndicator;

    protected void Update()
    {
        EnableGameOverPanel();
    }

    protected void EnableGameOverPanel()
    {
        if (playerHealth.isDead)
        {
            StartCoroutine(ONGameOverPanel());
            playerWeapons.SetActive(false);
            playerUI.SetActive(false);
        }
    }

    protected IEnumerator ONGameOverPanel()
    {
        FirstPersonController.Instance.mouseLook.m_cursorIsLocked = false;
        yield return new WaitForSeconds(0.1f);
        controller.enabled = false;
        yield return new WaitForSeconds(1.5f);
        Time.timeScale = 0;
        damageIndicator.SetActive(false);
        panel.SetActive(true);
    }
}

