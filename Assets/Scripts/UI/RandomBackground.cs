using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomBackground : MonoBehaviour
{
    [SerializeField] protected Image background;
    [SerializeField] protected Sprite[] backgroundImage;

    protected void Start()
    {
        background.sprite = backgroundImage[Random.Range(0, 3)];
    }
}
