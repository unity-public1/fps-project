using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePausePanel : MonoBehaviour
{
    [SerializeField] GameObject panel;
    [SerializeField] GameObject playerUI;
    [SerializeField] FirstPersonController playerController;
    protected bool enable;

    protected void Update()
    {
        Pause();
    }

    protected void Pause()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            FirstPersonController.Instance.mouseLook.XSensitivity = 0f;
            FirstPersonController.Instance.mouseLook.YSensitivity = 0f;
            playerUI.SetActive(false);
            panel.SetActive(true);
            Time.timeScale = 0;
        }
    }

    public void Resume()
    {
        FirstPersonController.Instance.mouseLook.XSensitivity = 2f;
        FirstPersonController.Instance.mouseLook.YSensitivity = 2f;
        playerUI.SetActive(true);
        panel.SetActive(false);
        Time.timeScale = 1;
    }

    public void Quit()
    {
        Application.Quit();
    }
}
