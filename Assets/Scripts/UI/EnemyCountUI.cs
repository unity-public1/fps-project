using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EnemyCountUI : MonoBehaviour
{
    [SerializeField] public TMP_Text UIText;

    protected void Update()
    {
        SetEnemyCountText();
    }

    public void SetEnemyCountText()
    {
        UIText.text = "Enemy remain  :  " + EnemyWaveController.Instance.enemyRemain.ToString();
    }
}
