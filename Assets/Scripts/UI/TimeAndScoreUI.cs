using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class TimeAndScoreUI : MonoBehaviour
{
    protected static TimeAndScoreUI instance;
    public static TimeAndScoreUI Instance => instance;

    [SerializeField] public TMP_Text timeAndScoreText;
    [SerializeField] public float timeCount;
    [SerializeField] public int scores = 0;
    protected int minutes => Mathf.FloorToInt(timeCount / 60f);
    protected int seconds => Mathf.FloorToInt(timeCount - minutes * 60);

    [HideInInspector] public string time => string.Format("{0:00}:{1:00}", minutes, seconds);

    protected void Awake() => CreateSingleton();

    protected void Update()
    {
        timeCount -= Time.deltaTime;

        SetTimeAndScoreText();
    }

    public void SetTimeAndScoreText()
    {
        timeAndScoreText.text = "Time  :  " + time + "  /  Score  :  " + scores.ToString("0000");
    }

    protected void CreateSingleton()
    {
        if (instance != null) return;
        instance = this;
    }
}
