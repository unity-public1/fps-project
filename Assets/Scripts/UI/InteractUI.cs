using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InteractUI : MonoBehaviour
{
    protected static InteractUI instance;
    public static InteractUI Instance => instance;

    [SerializeField] public GameObject holder;
    [SerializeField] public TMP_Text interactText;

    protected void Awake() => CreateSingleton();

    protected void CreateSingleton()
    {
        if (instance != null) return;
        instance = this;
    }
}
