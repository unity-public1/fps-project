using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapObjectClamp : MonoBehaviour
{
    [SerializeField] Transform minimapCamera => GameObject.Find("MinimapCamera").transform;
    [SerializeField] float minimapSize;
    [SerializeField] Vector3 temp;  

    protected void Update()
    {
        temp = transform.parent.transform.position;
        temp.y = transform.position.y;
        transform.position = temp;
    }

    protected void LateUpdate()
    {
        transform.position = new Vector3(
            Mathf.Clamp(transform.position.x, minimapCamera.position.x - minimapSize, minimapSize + minimapCamera.position.x),
            transform.position.y,
            Mathf.Clamp(transform.position.z, minimapCamera.position.z - minimapSize, minimapSize + minimapCamera.position.z)
            );
    }
}
