﻿using UnityEngine;
using System.Collections;

public class WaitAndDestroy : MonoBehaviour {

	[SerializeField] public float waitTime = 5f;

    protected void OnEnable()
    {
        BulletTracerSpawner.Instance.DespawnAfterTime(gameObject.transform, waitTime);
    }

    protected void Start () 
	{
        BulletTracerSpawner.Instance.DespawnAfterTime(gameObject.transform, waitTime);
    }
}
