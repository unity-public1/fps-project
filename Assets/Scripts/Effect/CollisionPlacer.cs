﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionPlacer : MonoBehaviour
{
    [SerializeField] protected LayerMask mask = ~0;
    [SerializeField] protected float autoPlaceMaxDistance = 6f;

    public void CollisionEnter(Collision collision)
    {
        Vector3 point = Vector3.zero;
        Vector3 normal = Vector3.zero;
        for (int i = 0; i < collision.contactCount; i++)
        {
            var c = collision.contacts[i];
            point += c.point;
            normal += c.normal;
        }

        point /= collision.contactCount;
        normal /= collision.contactCount;
        normal.Normalize();

        transform.position = point + normal * 0.01f;
        transform.rotation = Quaternion.AngleAxis(Random.value * 360, normal) * Quaternion.LookRotation(-normal);
    }

    public void AutoPlace()
    {
        Ray r = new Ray(transform.position, Vector3.down);
        RaycastHit hitInfo;

        if (Physics.Raycast(r, out hitInfo, autoPlaceMaxDistance, mask))
        {
            var point = hitInfo.point;
            var normal = hitInfo.normal;
            transform.position = point + normal * 0.01f;
            transform.rotation = Quaternion.AngleAxis(Random.value * 360, normal) * Quaternion.LookRotation(-normal);
        }
    }
}