using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "Weapon Data", menuName = "ScriptableObjects/Weapon Data")]

public class WeaponData : ScriptableObject
{
    [Header("Gun Mode")]
    public FireMode fireMode;
    public ReloadMode reloadMode;

    [Header("Damage")]
    public int minDamage;
    public int maxDamage;

    [Header("Ammo")]
    public int ammo;
    public int defaultAmmo;
    public int currentAmmo;
    public int defaultCurrentAmmo;
    public int magazineSize;

    [Header("Other")]
    public float bulletDistance;
    public float fireRate;

    [Header("Aiming")]
    public Vector3 normalPosition;
    public Vector3 aimPosition;

    [Header("Recoil")]
    public Vector3 normalRecoil;
    public Vector3 runningRecoil;
    public Vector3 aimRecoil;
    public float snappiness;
    public float returnSpeed;

    protected void OnEnable()
    {
#if UNITY_EDITOR
        // Subscribe for the "event"
        EditorApplication.playModeStateChanged += LogPlayModeState;
#endif
    }

    protected void OnDisable()
    {
#if UNITY_EDITOR
        // Don't forget to unsubscribe
        EditorApplication.playModeStateChanged -= LogPlayModeState;
#endif
    }

#if UNITY_EDITOR
    protected void LogPlayModeState(PlayModeStateChange state)
    {
        if (state == PlayModeStateChange.ExitingPlayMode)
        {
            // Set the value to default
            ammo = defaultAmmo;
            currentAmmo = defaultCurrentAmmo;
        }
    }
#endif
}
